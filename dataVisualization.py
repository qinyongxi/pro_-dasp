"""
    数据监测界面的独立py功能逻辑类，dataVisualization
"""

from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt, QSize, QDateTime, pyqtSlot
from PyQt5.QtWidgets import QWidget, QPushButton, QLabel, QMessageBox
from PyQt5.QtSql import QSqlTableModel
from matplotlib import pyplot as plt
from matplotlib.backends.backend_qtagg import FigureCanvasQTAgg

from ui_dataVisualization import Ui_dataVisualizationForm
# 导入全局变量
import globalVar as gl
gl._init()

class QmyDataVisualization(QWidget):
    def __init__(self, myDbOption, parent=None):
        super().__init__(parent)

        self.ui = Ui_dataVisualizationForm()
        self.ui.setupUi(self)
        self.ui.dataHistory_dateTimeEdit.dateTimeChanged.connect(self.on_dataHistory_dateTimeEdit_DateTimeChanged)

        self.dataTerminal_lists = []          # 根据config功能配置的数据监测的设备信息列表，每个元素为字典包括设备ID和名称（id、name）
        self.myDataDbClass = myDbOption
        self.dataTemperature_list = []        # 实时显示的温度数据
        self.dataHumidity_list = []           # 实时显示的湿度数据
        self.dataTime_list = []               # 数据时间的列表
        self.dataTimeDate = ''                # 数据时间的日期（显示到标题中）
        self.dataShowCount = gl.get_value("dataVisualizationCount")  # 界面显示数据个数容量

        self.DataNavigationChanged()               # 创建导航栏
        self.__initDataVisualization()             # 初始化创建画布

    # ----------------------------导航栏部分----------------------------------
    # 创建或更新导航栏
    def DataNavigationChanged(self):
        # 删除导航栏中原来的所有控件
        for i in range(self.ui.dataVisuBtnContainerFrame.layout().count()):
            self.ui.dataVisuBtnContainerFrame.layout().itemAt(i).widget().deleteLater()
        # 查询并添加需要加入的设备按键
        self.dataTerminal_lists = self.__getTerminal_info()
        if self.dataTerminal_lists is not None:
            for i in range(len(self.dataTerminal_lists)):
                btn_t = QPushButton(self.dataTerminal_lists[i]['name'], self)
                sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
                btn_t.setSizePolicy(sizePolicy)
                btn_t.setMinimumSize(QSize(0, 50))
                btn_t.setMaximumSize(QSize(16777215, 50))
                self.ui.dataVisuBtnContainerFrame.layout().addWidget(btn_t, 0, Qt.AlignTop)
        # 增加一个Lab下方空白导航进行填充，是的上面按键可以从上往下排列，并设置相同背景颜色
        commSpacerLab = QLabel(self)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Expanding)
        commSpacerLab.setSizePolicy(sizePolicy)
        commSpacerLab.setStyleSheet("QLabel{background-color: rgb(255, 255, 255);}")
        self.ui.dataVisuBtnContainerFrame.layout().addWidget(commSpacerLab)

    # 操作数据库表terminal_config获取已经启用的设备信息
    def __getTerminal_info(self):
        terminal_list = []
        tableModel = QSqlTableModel()           # 数据模型,用于查询数据
        if not self.myDataDbClass.db_table_check("terminal_config"):
            return
        tableModel.setTable("terminal_config")
        tableModel.setFilter("terminal_state=1")
        tableModel.select()
        for i in range(tableModel.rowCount()):
            terminal_dict = {'id': tableModel.record(i).value(0), 'name': tableModel.record(i).value(1)}
            terminal_list.append(terminal_dict)

        return terminal_list

    # 自定义槽函数，用于config更改设备状态的信号响应
    def do_signalTerminalStateChanged(self):
        self.DataNavigationChanged()      # 由于config设备状态可能更改发生信号，触发该槽函数更行导航栏

    # ----------------------------END----------------------------------------

    # ---------------------------绘图部分--------------------------------------
    # 初始化显示部分
    def __initDataVisualization(self):
        plt.rcParams['font.sans-serif'] = ["Simhei"]        # 设置默认字体,解决中文乱问问题
        plt.rcParams["axes.unicode_minus"] = False          # 坐标轴正确显示正负号

        self.currentData_temperature_humidity_get()      # 初始化读取当前实时最新数据
        self.init_matplotlib_temperature()                  # 初始化显示温度数据的画布
        self.ui.dataMatplotlibWidget.layout().addWidget(self.canvas_temperature)  # 将温度画布放入UI容器中

        # UI控件初始化处理（默认监测实时温度数据）
        self.ui.dataTemperatureShow_btn.setEnabled(False)
        self.ui.realTimeDataShow_btn.setEnabled(False)
        self.ui.dataCapacitySpinBox.setValue(self.dataShowCount)
        self.ui.dataHistory_dateTimeEdit.setDateTime(QDateTime.currentDateTime())
        self.ui.dataHistory_dateTimeEdit.setCalendarPopup(True)

    # 创建温度绘图窗体
    def init_matplotlib_temperature(self):
        self.fig_temperature = plt.figure()  # 创建画布
        self.canvas_temperature = FigureCanvasQTAgg(self.fig_temperature)  # 将一个figure渲染的canvas变为一个Qtwidgets
        self.ax_temperature = self.fig_temperature.subplots()  # 初始化matplotlib显示区域

        self.ax_temperature.spines['top'].set_visible(False)      # 顶边界不可见
        self.ax_temperature.spines['right'].set_visible(False)    # 右边界不可见
        self.ax_temperature.set_title('温度实时数据可视化('+self.dataTimeDate+')')
        self.ax_temperature.set_xlabel('时间', loc='right')
        self.ax_temperature.set_ylabel('℃', loc='top')
        self.ax_temperature.set_xlim(0, self.dataShowCount)
        self.ax_temperature.set_ylim(0, 50)
        plt.xticks(rotation=45)
        x_major_locator = plt.MultipleLocator(1)
        y_major_locator = plt.MultipleLocator(2)
        ax_gca = plt.gca()
        ax_gca.xaxis.set_major_locator(x_major_locator)
        ax_gca.yaxis.set_major_locator(y_major_locator)

        # 绘制湿度曲线
        plt.xticks(range(self.dataShowCount), self.dataTime_list)     # 重点，横坐标显示的label和值不同，详见xticks用法
        # 匹配横坐标的值，而不是label,所有参数用range(self.dataShowCount)
        self.line_temperature, = self.ax_temperature.plot(range(self.dataShowCount), self.dataTemperature_list)

    # 创建湿度绘图窗体
    def init_matplotlib_humidity(self):
        self.fig_humidity = plt.figure()  # 创建画布
        self.canvas_humidity = FigureCanvasQTAgg(self.fig_humidity)  #
        self.ax_humidity = self.fig_humidity.subplots()  # 初始化matplotlib显示区域

        self.ax_humidity.spines['top'].set_visible(False)      # 顶边界不可见
        self.ax_humidity.spines['right'].set_visible(False)    # 右边界不可见
        self.ax_humidity.set_title('湿度实时数据可视化('+self.dataTimeDate+')')
        self.ax_humidity.set_xlabel('时间', loc='right')
        self.ax_humidity.set_ylabel("湿度", loc='top')
        self.ax_humidity.set_xlim(0, self.dataShowCount)
        self.ax_humidity.set_ylim(20, 100)
        plt.xticks(rotation=45)
        x_major_locator = plt.MultipleLocator(1)
        y_major_locator = plt.MultipleLocator(4)
        ax = plt.gca()
        ax.xaxis.set_major_locator(x_major_locator)
        ax.yaxis.set_major_locator(y_major_locator)
        # 绘制湿度曲线
        plt.xticks(range(self.dataShowCount), self.dataTime_list)     # 重点，横坐标显示的label和值不同，详见xticks用法
        # 匹配横坐标的值，而不是label,所有参数用range(self.dataShowCount)
        self.line_humidity, = self.ax_humidity.plot(range(self.dataShowCount), self.dataHumidity_list)

    # 获取数据库当前最新的数据
    def currentData_temperature_humidity_get(self):
        nowDateTime = QDateTime.currentDateTime()
        self.__data_temperature_humidity_get(nowDateTime)

    # 获取指定时间的数据
    def __data_temperature_humidity_get(self, dateTime):
        getData_DateTime = dateTime.toString("yyyy-MM-dd hh:mm:ss")
        # 判断当月的数据表是否存在，若不存在则退出，只读取当月的数据表
        year = getData_DateTime[:4]
        month = getData_DateTime[5:7]
        data_Table_name = "data_{0}_{1}".format(year, month)  # 当月数据表名称，例如：data_2023_05
        if not self.myDataDbClass.db_table_check(data_Table_name):
            QMessageBox.information(self, "提示", "当前月无数据表存在!")
            return  # 当月表格不存在，无法采集数据，返回
        dataQueryModel = self.myDataDbClass.db_tableSqlQuery_temperature_humidity(data_Table_name, getData_DateTime, self.dataShowCount)
        self.dataTemperature_list.clear()  # 原有数据清除
        self.dataTime_list.clear()  # 原有数据清除
        self.dataHumidity_list.clear()  # 原有数据清除
        while dataQueryModel.next():
            self.dataTemperature_list.append(dataQueryModel.value(0))
            self.dataHumidity_list.append(dataQueryModel.value(1))
            datatime = dataQueryModel.value(2)[11:]
            self.dataTimeDate = dataQueryModel.value(2)[:10]
            self.dataTime_list.append(datatime)
    # ----------------------------END----------------------------------------

    # ---------------------------控件处理--------------------------------------
    # 温度切换按键
    @pyqtSlot()
    def on_dataTemperatureShow_btn_clicked(self):
        self.ui.dataTemperatureShow_btn.setEnabled(False)
        self.ui.dataHumidityShow_btn.setEnabled(True)
        self.ui.realTimeDataShow_btn.setEnabled(False)
        self.ui.historyDataShow_btn.setEnabled(True)
        for i in range(self.ui.dataMatplotlibWidget.layout().count()):
            self.ui.dataMatplotlibWidget.layout().itemAt(i).widget().deleteLater()
        self.init_matplotlib_temperature()
        self.ui.dataMatplotlibWidget.layout().addWidget(self.canvas_temperature)

    # 湿度切换按键
    @pyqtSlot()
    def on_dataHumidityShow_btn_clicked(self):
        self.ui.dataTemperatureShow_btn.setEnabled(True)
        self.ui.dataHumidityShow_btn.setEnabled(False)
        self.ui.realTimeDataShow_btn.setEnabled(False)
        self.ui.historyDataShow_btn.setEnabled(True)
        for i in range(self.ui.dataMatplotlibWidget.layout().count()):
            self.ui.dataMatplotlibWidget.layout().itemAt(i).widget().deleteLater()
        self.init_matplotlib_humidity()
        self.ui.dataMatplotlibWidget.layout().addWidget(self.canvas_humidity)

    # 实时监测启动按键
    @pyqtSlot()
    def on_realTimeDataShow_btn_clicked(self):
        self.ui.realTimeDataShow_btn.setEnabled(False)
        self.ui.historyDataShow_btn.setEnabled(True)
        self.do_signalCommDataReceive_in_storge()             # 出发一次最新数据显示

    # 历史数据监测启动按键
    @pyqtSlot()
    def on_historyDataShow_btn_clicked(self):
        self.ui.realTimeDataShow_btn.setEnabled(True)
        self.ui.historyDataShow_btn.setEnabled(False)
        datetime = self.ui.dataHistory_dateTimeEdit.dateTime()
        self.__data_temperature_humidity_get(datetime)                 # 获取历史数据到全局变量
        if not self.ui.dataHumidityShow_btn.isEnabled():               # 当前显示的数据为湿度
            self.ax_humidity.set_title('湿度历史数据可视化(' + self.dataTimeDate + ')')
            plt.xticks(range(self.dataShowCount), range(1, self.dataShowCount+1))
            if len(self.dataHumidity_list) != 0:
                plt.xticks(range(self.dataShowCount), self.dataTime_list)
                self.line_humidity.set_xdata(range(self.dataShowCount))    # 重点，横坐标显示的label和值不同，详见xticks用法
                self.line_humidity.set_ydata(self.dataHumidity_list)
            self.fig_humidity.canvas.draw()                                                             # 重新绘制画布
        if not self.ui.dataTemperatureShow_btn.isEnabled():            # 当前显示的数据为温度
            self.ax_temperature.set_title('温度历史数据可视化(' + self.dataTimeDate + ')')
            plt.xticks(range(self.dataShowCount), range(1, self.dataShowCount + 1))
            if len(self.dataTemperature_list) != 0:
                plt.xticks(range(self.dataShowCount), self.dataTime_list)
                self.line_temperature.set_xdata = (range(self.dataShowCount))
                self.line_temperature.set_ydata(self.dataTemperature_list)
            self.fig_temperature.canvas.draw()


        # ----------------------------END----------------------------------------

    # 实时更新数据槽函数,接受数据采集线程更新实时数据后发送的信号
    def do_signalCommDataReceive_in_storge(self):
        if self.ui.realTimeDataShow_btn.isEnabled():           # 实时监测按钮有效，目前处理历史数据显示状态，退出不实时更新
            return
        self.currentData_temperature_humidity_get()                    # 获取当前最新数据到全局变量
        if not self.ui.dataHumidityShow_btn.isEnabled():               # 当前显示的数据为湿度
            self.ax_humidity.set_title('湿度实时数据可视化(' + self.dataTimeDate + ')')
            plt.xticks(range(self.dataShowCount), range(1, self.dataShowCount+1))
            if len(self.dataHumidity_list) != 0:
                plt.xticks(range(self.dataShowCount), self.dataTime_list)
                self.line_humidity.set_xdata(range(self.dataShowCount))    # 重点，横坐标显示的label和值不同，详见xticks用法
                self.line_humidity.set_ydata(self.dataHumidity_list)
            self.fig_humidity.canvas.draw()                                                             # 重新绘制画布
        if not self.ui.dataTemperatureShow_btn.isEnabled():            # 当前显示的数据为温度
            self.ax_temperature.set_title('温度实时数据可视化(' + self.dataTimeDate + ')')
            plt.xticks(range(self.dataShowCount), range(1, self.dataShowCount + 1))
            if len(self.dataTemperature_list) != 0:
                plt.xticks(range(self.dataShowCount), self.dataTime_list)
                self.line_temperature.set_xdata = (range(self.dataShowCount))
                self.line_temperature.set_ydata(self.dataTemperature_list)
            self.fig_temperature.canvas.draw()                                                          # 重新绘制画布

    # 时间日期改变出发槽函数，更新历史监测数据
    def on_dataHistory_dateTimeEdit_DateTimeChanged(self, dateTime):
        if self.ui.historyDataShow_btn.isEnabled():                      # 当前不属于显示历史数据，返回
            return
        self.on_historyDataShow_btn_clicked()               # 代码启动执行一次历史数据查询更新

    # 显示数据数量的SpinBox发生变化后的槽函数处理
    @pyqtSlot(int)
    def on_dataCapacitySpinBox_valueChanged(self, value):
        gl.set_value("dataVisualizationCount", value)
        self.dataShowCount = gl.get_value("dataVisualizationCount")
