"""
    单体终端配置弹出界面逻辑类
"""
from PyQt5.QtWidgets import QDialog
from PyQt5.QtCore import Qt
from ui_configTerminal import Ui_configTerminalDialog

class QmyConfigTerminalDialog(QDialog):
    def __init__(self, name='', protocol='UDP', ip='0.0.0.0', port=1000, frequency=60, state=False, parent=None):
        super().__init__(parent)
        self.ui = Ui_configTerminalDialog()
        self.ui.setupUi(self)
        self.setWindowFlags(Qt.MSWindowsFixedSizeDialogHint)          # 固定窗口大小
        self.setIniData(name, protocol, ip, port, frequency, state)   # 初始化界面数据

    def __del__(self):
        print("QmyConfigTerminalDialog对象被删除了~")

    # ----------自定义功能函数---------------------
    # 初始化界面数据
    def setIniData(self, name, protocol, ip, port, frequency, state):
        self.ui.name_lineEdit.setText(name)
        if protocol == "UDP":
            self.ui.protocol_comboBox.setCurrentIndex(0)
        else:
            self.ui.protocol_comboBox.setCurrentIndex(1)
        self.ui.ip_lineEdit.setText(ip)
        self.ui.port_spinBox.setValue(port)
        self.ui.frequency_spinBox.setValue(frequency)
        if state:
            self.ui.state_checkBox.setChecked(True)
        else:
            self.ui.state_checkBox.setChecked(False)

    # 以元组形式反馈设置的数据
    def getSetData(self):
        name = self.ui.name_lineEdit.text()
        protocol = self.ui.protocol_comboBox.currentText()
        ip = self.ui.ip_lineEdit.text()
        port = self.ui.port_spinBox.value()
        frequency = self.ui.frequency_spinBox.value()
        state = self.ui.state_checkBox.isChecked()
        return name, protocol, ip, port, frequency, state
