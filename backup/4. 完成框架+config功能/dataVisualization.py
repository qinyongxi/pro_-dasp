"""
    数据监测界面的独立py功能逻辑类，dataVisualization
"""

from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt, QSize
from PyQt5.QtWidgets import QWidget, QPushButton, QLabel

from ui_dataVisualization import Ui_dataVisualizationForm

class QmyDataVisualization(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = Ui_dataVisualizationForm()
        self.ui.setupUi(self)

        for i in range(16):
            self.btn_t = QPushButton("高新区系统", self)
            sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
            self.btn_t.setSizePolicy(sizePolicy)
            self.btn_t.setMinimumSize(QSize(0, 50))
            self.btn_t.setMaximumSize(QSize(16777215, 50))
            self.ui.dataVisuBtnContainerFrame.layout().addWidget(self.btn_t, 0, Qt.AlignTop)
        # 增加一个Lab下方空白导航进行填充，是的上面按键可以从上往下排列，并设置相同背景颜色
        self.commSpacerLab = QLabel(self)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Expanding)
        self.commSpacerLab.setSizePolicy(sizePolicy)
        self.commSpacerLab.setStyleSheet("QLabel{background-color: rgb(255, 255, 255);}")
        self.ui.dataVisuBtnContainerFrame.layout().addWidget(self.commSpacerLab)
