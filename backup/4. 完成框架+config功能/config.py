"""
    配置界面的独立py功能逻辑类
"""

from PyQt5.QtCore import Qt, pyqtSlot
from PyQt5.QtWidgets import QWidget, QMessageBox, QDialog, QHeaderView
from PyQt5.QtSql import QSqlDatabase, QSqlQuery, QSqlQueryModel, QSqlTableModel

from ui_config import Ui_configForm
from configTerminal import QmyConfigTerminalDialog

class QmyConfig(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = Ui_configForm()
        self.ui.setupUi(self)

        self.myDbClass = QmyConfigDbOption()
        self.myDbClass.db_connect("./DAS.db")                            # 连接或创建数据库
        self.creator_table()                                             # 创建数据库表
        self.ui.configTableView.setModel(self.myDbClass.db_tableSqlQuery_show())   # 查询数据MODEL关联到UI组件上
        self.ui.configTableView.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)  # 表格自动填充满父容器
        # 第一列固定宽度
        self.ui.configTableView.horizontalHeader().setSectionResizeMode(0, QHeaderView.Fixed)
        self.ui.configTableView.setColumnWidth(0, 100)

    # 创建数据库表
    def creator_table(self):
        if not self.myDbClass.db_table_check("terminal_config"):
            self.myDbClass.db_QSqlQuery("CREATE TABLE [terminal_config](\
                                      [terminal_id] INTEGER PRIMARY KEY ASC AUTOINCREMENT NOT NULL,\
                                      [terminal_name] TEXT NOT NULL,\
                                      [terminal_protocol] TEXT,\
                                      [terminal_ip] TEXT NOT NULL,\
                                      [terminal_port] INTEGER NOT NULL,\
                                      [terminal_frequency] INTEGER,\
                                      [terminal_state] BOOL NOT NULL);")

        if not self.myDbClass.db_table_check("data_2023_5"):
            self.myDbClass.db_QSqlQuery("CREATE TABLE [data_2023_5](\
                                      [data_id] INTEGER PRIMARY KEY AUTOINCREMENT, \
                                      [terminal_id] INTEGER, \
                                      [data_temperature] FLOAT, \
                                      [data_humidity] FLOAT, \
                                      [data_time] DATE);")

    # 新增设备
    @pyqtSlot()
    def on_configAddTerminalBtn_clicked(self):
        configDialog = QmyConfigTerminalDialog()    # 局部变量，配置弹窗界面只存活本方法，不能传入self，新增数据用默认参数
        ret = configDialog.exec_()                  # 模态方式运行对话框
        if ret == QDialog.Accepted:
            name, protocol, ip, port, frequency, state = configDialog.getSetData()
            tableModel = QSqlTableModel()                                # 局部变量用于插入新的数据
            tableModel.setTable("terminal_config")
            tableModel.setEditStrategy(QSqlTableModel.OnManualSubmit)
            i = tableModel.rowCount()
            tableModel.insertRow(i)
            tableModel.setData(tableModel.index(i, 1), name)
            tableModel.setData(tableModel.index(i, 2), protocol)
            tableModel.setData(tableModel.index(i, 3), ip)
            tableModel.setData(tableModel.index(i, 4), port)
            tableModel.setData(tableModel.index(i, 5), frequency)
            tableModel.setData(tableModel.index(i, 6), state)

            tableModel.submitAll()
            self.ui.configTableView.setModel(self.myDbClass.db_tableSqlQuery_show())   # 新增数据更新到界面上

    # 删除设备
    @pyqtSlot()
    def on_configDelTerminalBtn_clicked(self):
        res = QMessageBox.question(self, "确认消息", "请确认是否删除当前选定设备", QMessageBox.Yes | QMessageBox.No)
        if res == QMessageBox.No:
            return
        currentIndex = self.ui.configTableView.currentIndex()
        tableModel = QSqlTableModel()  # 局部变量用于插入新的数据
        tableModel.setTable("terminal_config")
        tableModel.setEditStrategy(QSqlTableModel.OnManualSubmit)
        tableModel.select()                                                 # 无该语句，执行removeRow无法生效
        if tableModel.removeRow(currentIndex.row()):
            QMessageBox.information(self, "消息", "数据删除成功")

        tableModel.submitAll()
        self.ui.configTableView.setModel(self.myDbClass.db_tableSqlQuery_show())  # 新增数据更新到界面上

    # 修改设备
    @pyqtSlot()
    def on_configChangeTerminalBtn_clicked(self):
        row = self.ui.configTableView.currentIndex().row()         # 实现从tableView中获取一行数据
        if row < 0:
            return
        dataModel = self.ui.configTableView.model()
        name = dataModel.itemData(dataModel.index(row, 1))[0]
        protocol = dataModel.itemData(dataModel.index(row, 2))[0]
        ip = dataModel.itemData(dataModel.index(row, 3))[0]
        port = dataModel.itemData(dataModel.index(row, 4))[0]
        frequency = dataModel.itemData(dataModel.index(row, 5))[0]
        state = dataModel.itemData(dataModel.index(row, 6))[0]

        configDialog = QmyConfigTerminalDialog(name, protocol, ip, port, frequency, state)          # 局部变量
        ret = configDialog.exec_()
        if ret == QDialog.Accepted:             # 修改数据库
            name, protocol, ip, port, frequency, state = configDialog.getSetData()
            tableModel = QSqlTableModel()       # 局部变量用于修改数据库数据
            tableModel.setTable("terminal_config")
            tableModel.setEditStrategy(QSqlTableModel.OnManualSubmit)
            tableModel.select()                                      # 需要填充模型，不然无法修改到库中
            tableModel.setData(tableModel.index(row, 1), name)
            tableModel.setData(tableModel.index(row, 2), protocol)
            tableModel.setData(tableModel.index(row, 3), ip)
            tableModel.setData(tableModel.index(row, 4), port)
            tableModel.setData(tableModel.index(row, 5), frequency)
            tableModel.setData(tableModel.index(row, 6), state)
            tableModel.submitAll()

        self.ui.configTableView.setModel(self.myDbClass.db_tableSqlQuery_show())  # 新增数据更新到界面上

    # 退出界面时关闭数据库
    def closeEvent(self, QCloseEvent):
        self.myDbClass.db_close()


class QmyConfigDbOption(QSqlDatabase):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.db = ''
        self.tableSqlQueryModel = ''     # 用于查询展示的模型与UI的tableView关联

    # 链接或创建数据库，参数为带路径的数据库名称
    def db_connect(self, database_name_path):                  # 参数“./xxxx.db”
        self.db = QSqlDatabase.addDatabase('QSQLITE')        # 链接数据库类型为SQLITE
        self.db.setDatabaseName(database_name_path)            # databaseNamePath，如没有就新建
        if not self.db.open():
            QMessageBox.critical(self, 'Database Connection', self.db.lastError().text())

    # 检查数据表是否存在
    def db_table_check(self, table_name):
        table_name_list = self.db.tables()
        if table_name in table_name_list:
            return True
        else:
            return False

    # 根据SQL执行(这个方法没有用到self，故而写成了静态方法)
    @staticmethod
    def db_QSqlQuery(sql):
        creator = QSqlQuery()
        creator.exec_(sql)
        
    # 查询数据库表数据进行展示到UI
    def db_tableSqlQuery_show(self):
        self.tableSqlQueryModel = QSqlQueryModel()
        self.tableSqlQueryModel.setQuery("select *from terminal_config")        # 执行查询SQL
        self.tableSqlQueryModel.setHeaderData(0, Qt.Horizontal, '设备终端编号')
        self.tableSqlQueryModel.setHeaderData(1, Qt.Horizontal, '设备终端名称')
        self.tableSqlQueryModel.setHeaderData(2, Qt.Horizontal, '通信传输协议')
        self.tableSqlQueryModel.setHeaderData(3, Qt.Horizontal, '通信IP地址')
        self.tableSqlQueryModel.setHeaderData(4, Qt.Horizontal, '通信端口地址')
        self.tableSqlQueryModel.setHeaderData(5, Qt.Horizontal, '数据采集频率')
        self.tableSqlQueryModel.setHeaderData(6, Qt.Horizontal, '设备是否启用')

        return self.tableSqlQueryModel

    # 关闭数据库
    def db_close(self):
        self.db.close()
