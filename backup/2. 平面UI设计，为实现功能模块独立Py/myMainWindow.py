"""
    一个数据采集的CS软件，负责通过网络采集不同单片机（如ESP8266）等传感数据，并通过可视化组件进行显示。
    要求：采集设备数据量可配置、采集字段属性可配置、显示样式多样化。界面采用扁平化设计，类似微信PC的操作效果。
    主要功能：终端配置以及采集后台程序、采集数据实时通信监控、数据可视化展示三部分。
"""
import sys
from PyQt5.QtWidgets import (QApplication, QMainWindow)
from PyQt5.QtCore import Qt, QCoreApplication, pyqtSlot
# from PyQt5.QtWidgets import QStackedLayout, QGridLayout
from PyQt5.QtGui import QIcon

from ui_myMainWindow import Ui_myMainWindow

class QmyMainWindow(QMainWindow):
    def __init__(self, parent=None):
        super().__init__(parent)            # 调用父类构造函数，创建窗体
        self.ui = Ui_myMainWindow()         # 创建UI对象
        self.ui.setupUi(self)               # 构造UI

        self.ui.stackedWidget.setCurrentIndex(0)

        # 消除原有的程序标题栏
        self.setWindowFlag(Qt.FramelessWindowHint)
        # 设置侧面工具栏样式
        self.ui.toolBar.layout().setSpacing(20)           # 设置图标的间隔
        self.ui.toolBar.setFixedWidth(50)                 # 工具栏固定宽度

        # 设置TOP菜单栏三个按键的样式和间距
        self.ui.TopBarWidget.layout().setSpacing(20)
        self.ui.btnClose.setStyleSheet("border:none")
        self.ui.btnMaxWindow.setStyleSheet("border:none")
        self.ui.btnMinWindow.setStyleSheet("border:none")



    # ----------各个界面切换的槽函数--------------
    @pyqtSlot()
    def on_actHome_triggered(self):                         # 第1个按键，数据监测界面显示
        self.ui.stackedWidget.setCurrentIndex(0)
        self.toolBarBtn_ColorReset()                                    # 按键颜色全部恢复成白色
        self.ui.actHome.setIcon(QIcon(":/img/img/home_red.png"))        # 当前按键变为红色
    @pyqtSlot()
    def on_actData_Visualization_triggered(self):           # 第2个按键，数据监测界面显示
        self.ui.stackedWidget.setCurrentIndex(1)
        self.toolBarBtn_ColorReset()
        self.ui.actData_Visualization.setIcon(QIcon(":/img/img/DataChart_red.png"))
    @pyqtSlot()
    def on_actCommunication_monitoring_triggered(self):     # 第3个按键，通信监测界面显示
        self.ui.stackedWidget.setCurrentIndex(2)
        self.toolBarBtn_ColorReset()
        self.ui.actCommunication_monitoring.setIcon(QIcon(":/img/img/communication_red.png"))
    @pyqtSlot()
    def on_actConfig_NetAndData_triggered(self):            # 第4个按键，网络数据配置界面显示
        self.ui.stackedWidget.setCurrentIndex(3)
        self.toolBarBtn_ColorReset()
        self.ui.actConfig_NetAndData.setIcon(QIcon(":/img/img/config_red.png"))
    @pyqtSlot()
    def on_actMore_Func_triggered(self):
        pass
    def toolBarBtn_ColorReset(self):
        self.ui.actHome.setIcon(QIcon(":/img/img/home.png"))
        self.ui.actData_Visualization.setIcon(QIcon(":/img/img/DataChart.png"))
        self.ui.actCommunication_monitoring.setIcon(QIcon(":/img/img/communication.png"))
        self.ui.actConfig_NetAndData.setIcon(QIcon(":/img/img/config.png"))
        self.ui.actMore_Func.setIcon(QIcon(":/img/img/more.png"))


if __name__ == "__main__":
    QCoreApplication.setAttribute(Qt.AA_EnableHighDpiScaling)  # 解决2K分辨率下运行效果与Qt设计师显示不一致的问题
    app = QApplication(sys.argv)
    form = QmyMainWindow()
    form.show()
    sys.exit(app.exec_())
