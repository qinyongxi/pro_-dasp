# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dataVisualization.ui'
#
# Created by: PyQt5 UI code generator 5.15.9
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_dataVisualizationForm(object):
    def setupUi(self, dataVisualizationForm):
        dataVisualizationForm.setObjectName("dataVisualizationForm")
        dataVisualizationForm.resize(800, 400)
        self.verticalLayout = QtWidgets.QVBoxLayout(dataVisualizationForm)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setSpacing(0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.dataVisuBackgroundWidget = QtWidgets.QWidget(dataVisualizationForm)
        self.dataVisuBackgroundWidget.setObjectName("dataVisuBackgroundWidget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.dataVisuBackgroundWidget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setSpacing(0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.dataVisuNavigationWidget = QtWidgets.QWidget(self.dataVisuBackgroundWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.dataVisuNavigationWidget.sizePolicy().hasHeightForWidth())
        self.dataVisuNavigationWidget.setSizePolicy(sizePolicy)
        self.dataVisuNavigationWidget.setMinimumSize(QtCore.QSize(120, 0))
        self.dataVisuNavigationWidget.setMaximumSize(QtCore.QSize(150, 16777215))
        font = QtGui.QFont()
        font.setFamily("Agency FB")
        self.dataVisuNavigationWidget.setFont(font)
        self.dataVisuNavigationWidget.setObjectName("dataVisuNavigationWidget")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.dataVisuNavigationWidget)
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_2.setSpacing(0)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.dataVisuLabTitle = QtWidgets.QLabel(self.dataVisuNavigationWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.dataVisuLabTitle.sizePolicy().hasHeightForWidth())
        self.dataVisuLabTitle.setSizePolicy(sizePolicy)
        self.dataVisuLabTitle.setMinimumSize(QtCore.QSize(0, 50))
        self.dataVisuLabTitle.setMaximumSize(QtCore.QSize(16777215, 50))
        font = QtGui.QFont()
        font.setFamily("方正黑体_GBK")
        font.setPointSize(10)
        self.dataVisuLabTitle.setFont(font)
        self.dataVisuLabTitle.setStyleSheet("color: rgb(255, 255, 255);\n"
"background-color: rgb(60, 60, 60);")
        self.dataVisuLabTitle.setAlignment(QtCore.Qt.AlignCenter)
        self.dataVisuLabTitle.setObjectName("dataVisuLabTitle")
        self.verticalLayout_2.addWidget(self.dataVisuLabTitle)
        self.dataVisuBtnContainerScrollArea = QtWidgets.QScrollArea(self.dataVisuNavigationWidget)
        font = QtGui.QFont()
        font.setFamily("Agency FB")
        self.dataVisuBtnContainerScrollArea.setFont(font)
        self.dataVisuBtnContainerScrollArea.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.dataVisuBtnContainerScrollArea.setFrameShadow(QtWidgets.QFrame.Plain)
        self.dataVisuBtnContainerScrollArea.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.dataVisuBtnContainerScrollArea.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.dataVisuBtnContainerScrollArea.setWidgetResizable(True)
        self.dataVisuBtnContainerScrollArea.setAlignment(QtCore.Qt.AlignCenter)
        self.dataVisuBtnContainerScrollArea.setObjectName("dataVisuBtnContainerScrollArea")
        self.dataVisuBtnScrollAreaWidgetContents = QtWidgets.QWidget()
        self.dataVisuBtnScrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 150, 350))
        font = QtGui.QFont()
        font.setFamily("Agency FB")
        self.dataVisuBtnScrollAreaWidgetContents.setFont(font)
        self.dataVisuBtnScrollAreaWidgetContents.setObjectName("dataVisuBtnScrollAreaWidgetContents")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout(self.dataVisuBtnScrollAreaWidgetContents)
        self.verticalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_3.setSpacing(0)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.dataVisuBtnContainerFrame = QtWidgets.QFrame(self.dataVisuBtnScrollAreaWidgetContents)
        font = QtGui.QFont()
        font.setFamily("Agency FB")
        self.dataVisuBtnContainerFrame.setFont(font)
        self.dataVisuBtnContainerFrame.setFrameShape(QtWidgets.QFrame.Panel)
        self.dataVisuBtnContainerFrame.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.dataVisuBtnContainerFrame.setObjectName("dataVisuBtnContainerFrame")
        self.verticalLayout_4 = QtWidgets.QVBoxLayout(self.dataVisuBtnContainerFrame)
        self.verticalLayout_4.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_4.setSpacing(0)
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.verticalLayout_3.addWidget(self.dataVisuBtnContainerFrame)
        self.dataVisuBtnContainerScrollArea.setWidget(self.dataVisuBtnScrollAreaWidgetContents)
        self.verticalLayout_2.addWidget(self.dataVisuBtnContainerScrollArea)
        self.horizontalLayout.addWidget(self.dataVisuNavigationWidget)
        self.dataVisuContentWidget = QtWidgets.QWidget(self.dataVisuBackgroundWidget)
        self.dataVisuContentWidget.setObjectName("dataVisuContentWidget")
        self.horizontalLayout.addWidget(self.dataVisuContentWidget)
        self.verticalLayout.addWidget(self.dataVisuBackgroundWidget)

        self.retranslateUi(dataVisualizationForm)
        QtCore.QMetaObject.connectSlotsByName(dataVisualizationForm)

    def retranslateUi(self, dataVisualizationForm):
        _translate = QtCore.QCoreApplication.translate
        dataVisualizationForm.setWindowTitle(_translate("dataVisualizationForm", "Form"))
        self.dataVisuLabTitle.setText(_translate("dataVisualizationForm", "数据可视化"))
