'''
    数据通信界面的独立py功能逻辑类 communicationForm
'''

import sys
from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt, pyqtSlot, QSize
from PyQt5.QtWidgets import QPushButton, QWidget, QSizePolicy, QVBoxLayout, QSpacerItem,QLabel
from PyQt5.QtGui import QIcon, QFont

from ui_communication import Ui_communicationForm

class QmyCommunicationForm(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = Ui_communicationForm()
        self.ui.setupUi(self)
        # 导航栏初始化，实际项目中通过数据库获取，以及配置界面触发增加，用于选择显示通信的终端
        # self.commTerminal_list = [{'No':'1001', 'Name':'A终端名称'}, {'No':'1002', 'Name':'B终端名称'}, {'No':'1003', 'Name':'C终端名称'}]

        for i in range(6):
            self.btn_t = QPushButton("高新区系统", self)
            sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
            self.btn_t.setSizePolicy(sizePolicy)
            self.btn_t.setMinimumSize(QSize(0, 50))
            self.btn_t.setMaximumSize(QSize(16777215, 50))
            self.ui.commBtnContainerFrame.layout().addWidget(self.btn_t, 0, Qt.AlignTop)
        # 增加一个Lab下方空白导航进行填充，是的上面按键可以从上往下排列，并设置相同背景颜色
        self.commSpacerLab = QLabel(self)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Expanding)
        self.commSpacerLab.setSizePolicy(sizePolicy)
        self.commSpacerLab.setStyleSheet("QLabel{background-color: rgb(255, 255, 255);}")
        self.ui.commBtnContainerFrame.layout().addWidget(self.commSpacerLab)