'''
    配置界面的独立py功能逻辑类
'''

import sys
from PyQt5.QtCore import Qt, pyqtSlot
from PyQt5.QtWidgets import QStackedLayout, QGridLayout,QWidget
from PyQt5.QtGui import QIcon

from ui_config import Ui_configForm

class QmyConfig(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = Ui_configForm()
        self.ui.setupUi(self)




    @pyqtSlot()
    def on_btn_test_clicked(self):
        self.ui.label.setText("测试成果")
        self.ui.label_2.setText("测试成功")
        print("-----------")

        
