"""
    myWidget_2界面的类函数以及功能
"""

from PyQt5.QtCore import Qt, pyqtSlot
from PyQt5.QtWidgets import QWidget
from ui_myWidget_2 import Ui_myWidget_2


class QmyWidget_2(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)  # 调用父类构造函数，创建窗体
        self.ui = Ui_myWidget_2()  # 创建UI对象
        self.ui.setupUi(self)  # 构造UI
