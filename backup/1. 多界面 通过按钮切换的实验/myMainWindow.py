"""
    多界面 通过按钮切换的实验
"""
import sys
from PyQt5.QtWidgets import (QApplication, QMainWindow)
from PyQt5.QtCore import Qt, QCoreApplication, pyqtSlot
from PyQt5.QtWidgets import QStackedLayout, QWidget

from ui_myMainWindow import Ui_myMainWindow
from myWidget_1 import QmyWidget_1
from myWidget_2 import QmyWidget_2


class QmyMainWindow(QMainWindow):
    def __init__(self, parent=None):
        super().__init__(parent)            # 调用父类构造函数，创建窗体
        self.ui = Ui_myMainWindow()         # 创建UI对象
        self.ui.setupUi(self)               # 构造UI

        # 设置堆叠布局给frame组件
        self.qsl = QStackedLayout(self.ui.frame)
        myWidget1 = QmyWidget_1()
        myWidget2 = QmyWidget_2()
        self.qsl.addWidget(myWidget1)
        self.qsl.addWidget(myWidget2)

    # ----------各个界面切换的槽函数--------------
    @pyqtSlot()
    def on_actData_Visualization_triggered(self):
        self.qsl.setCurrentIndex(0)
    @pyqtSlot()
    def on_actCommunication_monitoring_triggered(self):
        self.qsl.setCurrentIndex(1)




if __name__ == "__main__":
    QCoreApplication.setAttribute(Qt.AA_EnableHighDpiScaling)  # 解决2K分辨率下运行效果与Qt设计师显示不一致的问题
    app = QApplication(sys.argv)
    form = QmyMainWindow()
    form.show()
    sys.exit(app.exec_())
