"""
    一个数据采集的CS软件，负责通过网络采集不同单片机（如ESP8266）等传感数据，并通过可视化组件进行显示。
    要求：
    （1）采集设备数据量可配置（实现多终端同时采集）、显示样式多样化。
    （2）界面采用扁平化设计，类似微信PC的操作界面风格。
    （3）主要功能：终端配置以及采集后台程序、采集数据实时通信监控、数据可视化展示三部分。
"""
import sys
from PyQt5.QtWidgets import (QApplication, QMainWindow)
from PyQt5.QtCore import Qt, QCoreApplication, pyqtSlot, QThread
from PyQt5.QtGui import QIcon
# Qt Designer界面导入
from ui_myMainWindow import Ui_myMainWindow
from dataVisualization import QmyDataVisualization
from communication import QmyCommunicationForm
from config import QmyConfig
# 导入数据库操作类
from dbOption import QmyDbOption
from communicationThread import QmyCommThread

class QmyMainWindow(QMainWindow):
    def __init__(self, parent=None):
        super().__init__(parent)            # 调用父类构造函数，创建窗体
        self.ui = Ui_myMainWindow()         # 创建UI对象
        self.ui.setupUi(self)               # 构造UI

        # 数据库对象有MainWindow建立，并传递个给子页面使用，全局一个数据库对象，避免错误
        self.myDbOption = QmyDbOption()
        self.myDbOption.db_connect("./DAS.db")      # 链接数据库，在config界面初始化建表

        # 消除原有的程序标题栏
        # self.setWindowFlag(Qt.FramelessWindowHint)

        # 设置侧面工具栏样式
        self.ui.toolBar.layout().setSpacing(20)           # 设置图标的间隔
        self.ui.toolBar.setFixedWidth(50)                 # 工具栏固定宽度
        self.ui.TopBarWidget.layout().setSpacing(20)      # 设置TOP菜单栏三个按键的样式和间距
        self.ui.btnClose.setStyleSheet("border:none")     # 自定义菜单栏的三个按键去除边框
        self.ui.btnMaxWindow.setStyleSheet("border:none")
        self.ui.btnMinWindow.setStyleSheet("border:none")

        # 首页，开机显示首页，首页不需要单独加载，首页完全有Qt Designer设计的静态页面，无任何功能
        self.ui.stackedWidget.setCurrentIndex(0)
        # 添加3个功能界面到stackedWidget三个页面的Layout中，实现三个主要功能mainWindow分离的编程结构，第一个界面是首页。
        self.dataVisualization = QmyDataVisualization(self.myDbOption)       # 数据可视化界面的逻辑类对象（包括了UI）
        self.communicationForm = QmyCommunicationForm(self.myDbOption)       # 数据通信监测界面的辑类对象（包括了UI）
        self.configPage = QmyConfig(self.myDbOption)                         # 数据通信终端配置界面的辑类对象（包括了UI）
        # dataVisualization_verticalLayout 在Qt Designer设计中stackedWidget第二个界面的布局
        self.ui.dataVisualization_verticalLayout.addWidget(self.dataVisualization)
        # communicationMonitoring_VerticalLayout在Qt Designer设计中stackedWidget第三个界面的布局
        self.ui.communicationMonitoring_VerticalLayout.addWidget(self.communicationForm)
        # config_verticalLayout在Qt Designer设计中stackedWidget第四个界面的布局
        self.ui.config_verticalLayout.addWidget(self.configPage)

        # 设置界面之间的通信机制（信号与槽的关联）,一个信号连接来个页面共2个槽函数，实现设备状态变化触发2个页面的导航栏变化功能
        self.configPage.signalTerminalStateChanged.connect(self.communicationForm.do_signalTerminalStateChanged)
        self.configPage.signalTerminalStateChanged.connect(self.dataVisualization.do_signalTerminalStateChanged)

        # 启动数据采集子线程任务
        self.commThread = QThread()
        # 采集线程的类对象
        self.commThreadClass = QmyCommThread(self.communicationForm.commTerminal_lists, self.communicationForm.commTerminal_lists_valid_id, self.myDbOption)
        self.setup_CommThread()

    # ----------侧面工具栏5个Action的槽函数，实现不同功能界面的切换--------------
    @pyqtSlot()                                                         # 第1个按键，首页
    def on_actHome_triggered(self):
        self.ui.stackedWidget.setCurrentIndex(0)
        self.__toolBarBtn_ColorReset()                                  # 按键颜色全部恢复成白色
        self.ui.actHome.setIcon(QIcon(":/img/img/home_red.png"))        # 当前按键变为红色
    @pyqtSlot()                                                         # 第2个按键，数据监测界面显示
    def on_actData_Visualization_triggered(self):
        self.ui.stackedWidget.setCurrentIndex(1)
        self.__toolBarBtn_ColorReset()
        self.ui.actData_Visualization.setIcon(QIcon(":/img/img/DataChart_red.png"))
        self.on_actMore_Func_triggered(True)         # 切换功能页面时，默认恢复侧边栏
        self.ui.actMore_Func.setChecked(True)      # 切换功能页面时，默认恢复更多选择有效
    @pyqtSlot()                                                         # 第3个按键，通信监测界面显示
    def on_actCommunication_monitoring_triggered(self):
        self.ui.stackedWidget.setCurrentIndex(2)
        self.__toolBarBtn_ColorReset()
        self.ui.actCommunication_monitoring.setIcon(QIcon(":/img/img/communication_red.png"))
        self.on_actMore_Func_triggered(True)
        self.ui.actMore_Func.setChecked(True)
    @pyqtSlot()                                                         # 第4个按键，网络数据配置界面显示
    def on_actConfig_NetAndData_triggered(self):
        self.ui.stackedWidget.setCurrentIndex(3)
        self.__toolBarBtn_ColorReset()
        self.ui.actConfig_NetAndData.setIcon(QIcon(":/img/img/config_red.png"))
        self.on_actMore_Func_triggered(True)
        self.ui.actMore_Func.setChecked(True)
    @pyqtSlot(bool)                                                     # 第5个按键，无界面，实现前面3个页面的菜单栏隐藏功能
    def on_actMore_Func_triggered(self, checked):
        # 通过界面逻辑类对象调用，外部界面的ui组件进行操作。
        self.communicationForm.ui.commNavigationWidget.setVisible(checked)
        self.configPage.ui.configNavigationWidget.setVisible(checked)
        self.dataVisualization.ui.dataVisuNavigationWidget.setVisible(checked)
    def __toolBarBtn_ColorReset(self):                                  # 本部分的内部功能函数
        # 该部分的内部函数，实现所有侧工具栏按键颜色恢复白色
        self.ui.actHome.setIcon(QIcon(":/img/img/home.png"))
        self.ui.actData_Visualization.setIcon(QIcon(":/img/img/DataChart.png"))
        self.ui.actCommunication_monitoring.setIcon(QIcon(":/img/img/communication.png"))
        self.ui.actConfig_NetAndData.setIcon(QIcon(":/img/img/config.png"))
        self.ui.actMore_Func.setIcon(QIcon(":/img/img/more.png"))
    # -----------------------END--------------------------

    def closeEvent(self, QCloseEvent):
        self.myDbOption.db_close()

    # -----------------------采集任务子线程--------------------------
    def setup_CommThread(self):
        self.commThreadClass.moveToThread(self.commThread)  # 将线程类move到线程中
        # 信号与槽的关联
        self.commThread.started.connect(self.commThreadClass.run)    # 启动关联
        self.commThread.finished.connect(self.commThread.deleteLater)
        # 关联线程见通信的信号与槽，采集任务中的信息通过该链接传递数据到通信的UI界面上
        self.commThreadClass.signalCommProcess_toUI.connect(self.communicationForm.do_signalCommProcess_toUI)

        self.commThread.start()   # 启动线程

    # ----------------------------END-----------------------------

if __name__ == "__main__":
    QCoreApplication.setAttribute(Qt.AA_EnableHighDpiScaling)  # 解决2K分辨率下运行效果与Qt设计师显示不一致的问题
    app = QApplication(sys.argv)
    form = QmyMainWindow()
    form.show()
    sys.exit(app.exec_())
