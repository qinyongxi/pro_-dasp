"""
    配置界面的独立py功能逻辑类
"""
from PyQt5.QtCore import pyqtSlot, pyqtSignal
from PyQt5.QtWidgets import QWidget, QMessageBox, QDialog, QHeaderView
from PyQt5.QtSql import QSqlTableModel

from ui_config import Ui_configForm
from configTerminal import QmyConfigTerminalDialog


class QmyConfig(QWidget):
    signalTerminalStateChanged = pyqtSignal()  # 当配置设备状态发生变化时，向其他页面发生信号，该表其他页面的导航栏

    def __init__(self, myDbOption, parent=None):
        super().__init__(parent)
        self.ui = Ui_configForm()
        self.ui.setupUi(self)

        self.myDbClass = myDbOption
        self.creator_table()                                             # 创建数据库表
        self.ui.configTableView.setModel(self.myDbClass.db_tableSqlQuery_show())   # 查询数据MODEL关联到UI组件上
        self.ui.configTableView.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)  # 表格自动填充满父容器
        # 第一列固定宽度
        self.ui.configTableView.horizontalHeader().setSectionResizeMode(0, QHeaderView.Fixed)
        self.ui.configTableView.setColumnWidth(0, 100)

    # 创建数据库表
    def creator_table(self):
        if not self.myDbClass.db_table_check("terminal_config"):
            self.myDbClass.db_QSqlQuery("CREATE TABLE [terminal_config](\
                                      [terminal_id] INTEGER PRIMARY KEY ASC AUTOINCREMENT NOT NULL,\
                                      [terminal_name] TEXT NOT NULL,\
                                      [terminal_protocol] TEXT,\
                                      [terminal_ip] TEXT NOT NULL,\
                                      [terminal_port] INTEGER NOT NULL,\
                                      [terminal_frequency] INTEGER,\
                                      [terminal_state] BOOL NOT NULL);")

        if not self.myDbClass.db_table_check("data_2023_5"):
            self.myDbClass.db_QSqlQuery("CREATE TABLE [data_2023_5]( \
                                      [data_id] INTEGER PRIMARY KEY ASC AUTOINCREMENT NOT NULL, \
                                      [terminal_id] INTEGER, \
                                      [data_temperature] FLOAT, \
                                      [data_humidity] FLOAT, \
                                      [data_time] TEXT);")

    # 新增设备
    @pyqtSlot()
    def on_configAddTerminalBtn_clicked(self):
        configDialog = QmyConfigTerminalDialog()    # 局部变量，配置弹窗界面只存活本方法，不能传入self，新增数据用默认参数
        ret = configDialog.exec_()                  # 模态方式运行对话框
        if ret == QDialog.Accepted:
            name, protocol, ip, port, frequency, state = configDialog.getSetData()
            tableModel = QSqlTableModel(self, self.myDbClass.db)                                # 局部变量用于插入新的数据
            tableModel.setTable("terminal_config")
            tableModel.select()
            tableModel.setEditStrategy(QSqlTableModel.OnManualSubmit)
            rowCount = tableModel.rowCount()
            tableModel.insertRow(rowCount)
            tableModel.setData(tableModel.index(rowCount, 1), name)
            tableModel.setData(tableModel.index(rowCount, 2), protocol)
            tableModel.setData(tableModel.index(rowCount, 3), ip)
            tableModel.setData(tableModel.index(rowCount, 4), port)
            tableModel.setData(tableModel.index(rowCount, 5), frequency)
            tableModel.setData(tableModel.index(rowCount, 6), state)
            tableModel.submitAll()
            self.ui.configTableView.setModel(self.myDbClass.db_tableSqlQuery_show())   # 新增数据更新到界面上
            self.signalTerminalStateChanged.emit()    # 设备可能发生变化，发射信号


    # 删除设备
    @pyqtSlot()
    def on_configDelTerminalBtn_clicked(self):
        res = QMessageBox.question(self, "确认消息", "请确认是否删除当前选定设备", QMessageBox.Yes | QMessageBox.No)
        if res == QMessageBox.No:
            return
        currentIndex = self.ui.configTableView.currentIndex()
        tableModel = QSqlTableModel(self, self.myDbClass.db)      # 局部变量用于插入新的数据
        tableModel.setTable("terminal_config")
        tableModel.setEditStrategy(QSqlTableModel.OnManualSubmit)
        tableModel.select()                                                 # 无该语句，执行removeRow无法生效
        if tableModel.removeRow(currentIndex.row()):
            QMessageBox.information(self, "消息", "数据删除成功")

        tableModel.submitAll()
        self.ui.configTableView.setModel(self.myDbClass.db_tableSqlQuery_show())  # 新增数据更新到界面上
        self.signalTerminalStateChanged.emit()  # 设备可能发生变化，发射信号

    # 修改设备
    @pyqtSlot()
    def on_configChangeTerminalBtn_clicked(self):
        row = self.ui.configTableView.currentIndex().row()         # 实现从tableView中获取一行数据
        if row < 0:
            QMessageBox.information(self, "提示消息", "请选择一条需要修改的设备数据", QMessageBox.Yes)
            return
        dataModel = self.ui.configTableView.model()
        parameterList = []      # 将6个参数放入list中
        for i in range(6):
            parameterList.append(dataModel.itemData(dataModel.index(row, i+1))[0])
        # 调用子窗口，输入当前值作为初始值
        configDialog = QmyConfigTerminalDialog(parameterList[0], parameterList[1], parameterList[2],
                                               parameterList[3], parameterList[4], parameterList[5])
        ret = configDialog.exec_()
        if ret == QDialog.Accepted:             # 修改数据库
            name, protocol, ip, port, frequency, state = configDialog.getSetData()
            tableModel = QSqlTableModel(self, self.myDbClass.db)       # 局部变量用于修改数据库数据
            tableModel.setTable("terminal_config")
            tableModel.setEditStrategy(QSqlTableModel.OnManualSubmit)
            tableModel.select()                                      # 需要填充模型，不然无法修改到库中
            tableModel.setData(tableModel.index(row, 1), name)
            tableModel.setData(tableModel.index(row, 2), protocol)
            tableModel.setData(tableModel.index(row, 3), ip)
            tableModel.setData(tableModel.index(row, 4), port)
            tableModel.setData(tableModel.index(row, 5), frequency)
            tableModel.setData(tableModel.index(row, 6), state)
            tableModel.submitAll()

        self.ui.configTableView.setModel(self.myDbClass.db_tableSqlQuery_show())  # 新增数据更新到界面上
        self.signalTerminalStateChanged.emit()  # 设备可能发生变化，发射信号
