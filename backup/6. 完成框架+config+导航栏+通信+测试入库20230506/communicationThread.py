"""
    数据采集任务，单独的线程进行数据采集
"""

from PyQt5.QtCore import QThread, pyqtSignal, QDateTime, pyqtSlot, QMutex
from PyQt5.QtNetwork import QUdpSocket, QHostAddress
from PyQt5.QtSql import QSqlTableModel


myQmutex = QMutex()          # 全局互斥变量

class QmyCommThread(QThread):
    signalThread_crashed = pyqtSignal(Exception)      # 定义一个线程崩溃信号
    signalCommProcess_toUI = pyqtSignal(str)          # 定义一个通信信号，将数据采集过程中需要再界面上显示的信息传递给界面线程
    def __init__(self, commTerminal_lists, commTerminal_lists_valid_id, myDbOption, parent=None):
        super().__init__(parent)

        self.commTerminal_lists = commTerminal_lists                      # 当前有效监测设备的信息
        self.commTerminal_lists_valid_id = commTerminal_lists_valid_id    # 当前启动的监测设备的id信息
        self.__currentCommTerminal_id = 0                                 # 存储当前正在采集的设备id信息
        self.__currentCommTerminal_name = ''                              # 存储当前正在采集的设备名称信息

        self.myDbClass = myDbOption
        self.updSocket = QUdpSocket(self)

    def run(self):
        try:
            while True:
                self.do_dataCommProcess()
                QThread.sleep(5)

        except Exception as e:
            # 发生线程崩溃的信号
            self.signalCommProcess_toUI.emit(e)
            self.signalThread_crashed.emit(e)

    def do_dataCommProcess(self):
        nowTime = QDateTime.currentDateTime()
        self.signalCommProcess_toUI.emit("子线程任务处理时间：{0}".format(nowTime.toString("yyyy-MM-dd hh:mm:ss")))
        for i in range(len(self.commTerminal_lists_valid_id)):      # 循环实现对多个设备数据的采集
            self.__currentCommTerminal_id = self.commTerminal_lists_valid_id[i]
            self.__currentCommTerminal_name, comm_ip, comm_port = self.__find_name_ip_port(self.__currentCommTerminal_id)
            self.__dataCollect_Process(comm_ip, comm_port)


    # 查询采集设备的名称、ip和port值
    def __find_name_ip_port(self, valid_id):
        for i in range(len(self.commTerminal_lists)):
            if valid_id == self.commTerminal_lists[i]['id']:
                return self.commTerminal_lists[i]['name'], self.commTerminal_lists[i]['ip'], self.commTerminal_lists[i]['port']

    # 单个设备数据采集处理
    def __dataCollect_Process(self, remote_ip, remote_port):
            # 注意要先配置接收，再发送数据，如果反过来则无法接收到数据
            self.updSocket = QUdpSocket(self)
            self.updSocket.bind(QHostAddress(remote_ip), remote_port)
            self.updSocket.readyRead.connect(self.on_readRead)

            data = "LED_ON".encode()
            self.updSocket.writeDatagram(data, QHostAddress(remote_ip), remote_port)
            self.signalCommProcess_toUI.emit("发送采集请求，请求设备：{0}".format(self.__currentCommTerminal_name))
            self.updSocket.waitForReadyRead(200)                     # 阻塞200ms等待接收数据
            self.updSocket.close()

            self.__test_data_collect()    # 模拟测试数据入库

    # 网络UPD接收数据
    @pyqtSlot()
    def on_readRead(self):
        while self.updSocket.hasPendingDatagrams():       # 判断是否接收到数据
            data, host, port = self.updSocket.readDatagram(self.updSocket.pendingDatagramSize())
            self.signalCommProcess_toUI.emit("接收到IP：{0}, 端口：{1}，发送的数据：{2}".format(host.toString(), str(port), data.decode()))

        self.signalCommProcess_toUI.emit("rev data OK!")

    def __test_data_collect(self):
        # 模拟数据并入库
        temperature = 20
        humidity = 65
        nowDateTime = QDateTime.currentDateTime().toString("yyyy-MM-dd hh:mm:ss")
        self.signalCommProcess_toUI.emit("接收到IP：{0}, 端口：{1}，发送的数据：{2}".format("模拟127.0.0.1", "模拟2000", "temperature:20,  humidity: 65"))

        myQmutex.lock()      # 操作数据库加锁

        data_tableModel = QSqlTableModel(self, self.myDbClass.db)          # 局部变量用于插入新的数据
        data_tableModel.setTable("data_2023_5")
        data_tableModel.setEditStrategy(QSqlTableModel.OnManualSubmit)
        data_tableModel.select()
        data_rowCount = data_tableModel.rowCount()
        data_tableModel.insertRow(data_rowCount)
        data_tableModel.setData(data_tableModel.index(data_rowCount, 1), int(self.__currentCommTerminal_id))
        data_tableModel.setData(data_tableModel.index(data_rowCount, 2), float(temperature))
        data_tableModel.setData(data_tableModel.index(data_rowCount, 3), float(humidity))
        data_tableModel.setData(data_tableModel.index(data_rowCount, 4), nowDateTime)
        if data_tableModel.submitAll():
            data_tableModel.database().commit()
            self.signalCommProcess_toUI.emit("采集数据入库成功: 设备：{0}，温度{1}，湿度{2}，时间{3}".format(self.__currentCommTerminal_name, temperature, humidity, nowDateTime))
        else:
            self.signalCommProcess_toUI.emit("采集数据入库失败：设备：{0}".format(self.__currentCommTerminal_name))

        myQmutex.unlock()     # 操作数据库完成解锁
