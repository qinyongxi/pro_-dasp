# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'configTerminal.ui'
#
# Created by: PyQt5 UI code generator 5.15.9
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_configTerminalDialog(object):
    def setupUi(self, configTerminalDialog):
        configTerminalDialog.setObjectName("configTerminalDialog")
        configTerminalDialog.resize(324, 272)
        self.verticalLayout = QtWidgets.QVBoxLayout(configTerminalDialog)
        self.verticalLayout.setContentsMargins(1, 1, 1, 1)
        self.verticalLayout.setSpacing(1)
        self.verticalLayout.setObjectName("verticalLayout")
        self.groupBox = QtWidgets.QGroupBox(configTerminalDialog)
        self.groupBox.setTitle("")
        self.groupBox.setObjectName("groupBox")
        self.formLayout = QtWidgets.QFormLayout(self.groupBox)
        self.formLayout.setContentsMargins(9, -1, -1, -1)
        self.formLayout.setVerticalSpacing(15)
        self.formLayout.setObjectName("formLayout")
        self.label = QtWidgets.QLabel(self.groupBox)
        self.label.setObjectName("label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label)
        self.name_lineEdit = QtWidgets.QLineEdit(self.groupBox)
        self.name_lineEdit.setObjectName("name_lineEdit")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.name_lineEdit)
        self.label_2 = QtWidgets.QLabel(self.groupBox)
        self.label_2.setObjectName("label_2")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_2)
        self.label_3 = QtWidgets.QLabel(self.groupBox)
        self.label_3.setObjectName("label_3")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.label_3)
        self.ip_lineEdit = QtWidgets.QLineEdit(self.groupBox)
        self.ip_lineEdit.setObjectName("ip_lineEdit")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.ip_lineEdit)
        self.label_4 = QtWidgets.QLabel(self.groupBox)
        self.label_4.setObjectName("label_4")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.label_4)
        self.port_spinBox = QtWidgets.QSpinBox(self.groupBox)
        self.port_spinBox.setMinimum(1000)
        self.port_spinBox.setMaximum(5000)
        self.port_spinBox.setStepType(QtWidgets.QAbstractSpinBox.DefaultStepType)
        self.port_spinBox.setObjectName("port_spinBox")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.port_spinBox)
        self.label_5 = QtWidgets.QLabel(self.groupBox)
        self.label_5.setObjectName("label_5")
        self.formLayout.setWidget(4, QtWidgets.QFormLayout.LabelRole, self.label_5)
        self.label_6 = QtWidgets.QLabel(self.groupBox)
        self.label_6.setObjectName("label_6")
        self.formLayout.setWidget(5, QtWidgets.QFormLayout.LabelRole, self.label_6)
        self.frequency_spinBox = QtWidgets.QSpinBox(self.groupBox)
        self.frequency_spinBox.setMinimum(1)
        self.frequency_spinBox.setMaximum(60)
        self.frequency_spinBox.setObjectName("frequency_spinBox")
        self.formLayout.setWidget(4, QtWidgets.QFormLayout.FieldRole, self.frequency_spinBox)
        self.state_checkBox = QtWidgets.QCheckBox(self.groupBox)
        self.state_checkBox.setText("")
        self.state_checkBox.setObjectName("state_checkBox")
        self.formLayout.setWidget(5, QtWidgets.QFormLayout.FieldRole, self.state_checkBox)
        self.protocol_comboBox = QtWidgets.QComboBox(self.groupBox)
        self.protocol_comboBox.setObjectName("protocol_comboBox")
        self.protocol_comboBox.addItem("")
        self.protocol_comboBox.addItem("")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.protocol_comboBox)
        self.verticalLayout.addWidget(self.groupBox)
        self.buttonBox = QtWidgets.QDialogButtonBox(configTerminalDialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)

        self.retranslateUi(configTerminalDialog)
        self.buttonBox.accepted.connect(configTerminalDialog.accept) # type: ignore
        self.buttonBox.rejected.connect(configTerminalDialog.reject) # type: ignore
        QtCore.QMetaObject.connectSlotsByName(configTerminalDialog)

    def retranslateUi(self, configTerminalDialog):
        _translate = QtCore.QCoreApplication.translate
        configTerminalDialog.setWindowTitle(_translate("configTerminalDialog", "单终端参数配置"))
        self.label.setText(_translate("configTerminalDialog", "设备终端名称"))
        self.label_2.setText(_translate("configTerminalDialog", "通信传输协议"))
        self.label_3.setText(_translate("configTerminalDialog", "通信IP地址"))
        self.label_4.setText(_translate("configTerminalDialog", "通信端口地址"))
        self.label_5.setText(_translate("configTerminalDialog", "数据采集频率"))
        self.label_6.setText(_translate("configTerminalDialog", "设备是否启用"))
        self.protocol_comboBox.setItemText(0, _translate("configTerminalDialog", "UDP"))
        self.protocol_comboBox.setItemText(1, _translate("configTerminalDialog", "TCP"))
