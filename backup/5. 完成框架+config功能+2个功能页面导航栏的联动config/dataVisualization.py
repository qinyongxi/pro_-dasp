"""
    数据监测界面的独立py功能逻辑类，dataVisualization
"""

from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt, QSize
from PyQt5.QtWidgets import QWidget, QPushButton, QLabel
from PyQt5.QtSql import QSqlTableModel

from ui_dataVisualization import Ui_dataVisualizationForm

class QmyDataVisualization(QWidget):
    def __init__(self, myDbOption, parent=None):
        super().__init__(parent)
        self.ui = Ui_dataVisualizationForm()
        self.ui.setupUi(self)

        self.dataTerminal_lists = []  # 根据config功能配置的数据监测的设备信息列表，每个元素为字典包括设备ID和名称（id、name）
        self.myDataDbClass = myDbOption
        self.DataNavigationChanged()  # 创建导航栏

    # 创建或更新导航栏
    def DataNavigationChanged(self):
        # 删除导航栏中原来的所有控件
        for i in range(self.ui.dataVisuBtnContainerFrame.layout().count()):
            self.ui.dataVisuBtnContainerFrame.layout().itemAt(i).widget().deleteLater()
        # 查询并添加需要加入的设备按键
        self.dataTerminal_lists = self.__getTerminal_info()
        for i in range(len(self.dataTerminal_lists)):
            btn_t = QPushButton(self.dataTerminal_lists[i]['name'], self)
            sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
            btn_t.setSizePolicy(sizePolicy)
            btn_t.setMinimumSize(QSize(0, 50))
            btn_t.setMaximumSize(QSize(16777215, 50))
            self.ui.dataVisuBtnContainerFrame.layout().addWidget(btn_t, 0, Qt.AlignTop)
        # 增加一个Lab下方空白导航进行填充，是的上面按键可以从上往下排列，并设置相同背景颜色
        commSpacerLab = QLabel(self)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Expanding)
        commSpacerLab.setSizePolicy(sizePolicy)
        commSpacerLab.setStyleSheet("QLabel{background-color: rgb(255, 255, 255);}")
        self.ui.dataVisuBtnContainerFrame.layout().addWidget(commSpacerLab)

    # 操作数据库表terminal_config获取已经启用的设备信息
    def __getTerminal_info(self):
        terminal_list = []
        tableModel = QSqlTableModel()           # 数据模型,用于查询数据
        if not self.myDataDbClass.db_table_check("terminal_config"):
            return
        tableModel.setTable("terminal_config")
        tableModel.setFilter("terminal_state=1")
        tableModel.select()
        for i in range(tableModel.rowCount()):
            terminal_dict = {'id': tableModel.record(i).value(0), 'name': tableModel.record(i).value(1)}
            terminal_list.append(terminal_dict)

        return terminal_list

    # 自定义槽函数，用于config更改设备状态的信号响应
    def do_signalTerminalStateChanged(self):
        self.DataNavigationChanged()      # 由于config设备状态可能更改发生信号，触发该槽函数更行导航栏