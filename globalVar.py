"""
    全局变量py文件，存储全局配置变量
"""
# 初始化全局变量
def _init():
    global _global_dict
    _global_dict = {
        "dataCollectTaskLoop_time": 5,                               # 采集数据间隔时间，单位s, 10分钟=600s
        "terminalCollectInterval_time": 1000,                        # 一次采集多个设备，设备采集间隔时间，单位ms
        "waitForReadyRead_time": 2000,                               # 采集指令下发后，阻塞等到接收的时间，单位ms

        "dataVisualizationCount": 60                                 # 数据监测页面界面显示数据个数容量
    }
# 设置全局变量数据
def set_value(key, value):
    _global_dict[key] = value
# 获取全局变量数据
def get_value(key):
    try:
        return _global_dict[key]
    except KeyError:
        return "Key invalid"