"""
    数据库操作类文件
"""
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QMessageBox
from PyQt5.QtSql import QSqlDatabase, QSqlQuery, QSqlQueryModel

class QmyDbOption(QSqlDatabase):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.db = ''
        self.tableSqlQueryModel = ''  # 用于查询展示的模型与UI的tableView关联
        self.dataSqlQueryModel = ''   # 用于查询监测数据，温度和湿度值
        self.dataQuery = None

    # 链接或创建数据库，参数为带路径的数据库名称
    def db_connect(self, database_name_path):  # 参数“./xxxx.db”
        self.db = QSqlDatabase.addDatabase('QSQLITE')  # 链接数据库类型为SQLITE
        self.db.setDatabaseName(database_name_path)  # databaseNamePath，如没有就新建
        if not self.db.open():
            QMessageBox.critical(self, 'Database Connection', self.db.lastError().text())

    # 检查数据表是否存在
    def db_table_check(self, table_name):
        table_name_list = self.db.tables()
        if table_name in table_name_list:
            return True
        else:
            return False

    # 根据SQL执行(这个方法没有用到self，故而写成了静态方法)
    def db_QSqlQuery(self, sql):
        creator = QSqlQuery(self.db)
        creator.exec_(sql)

    # 查询数据库表数据进行展示到UI（配置数据）
    def db_tableSqlQuery_show(self):
        self.tableSqlQueryModel = QSqlQueryModel()
        self.tableSqlQueryModel.setQuery("select *from terminal_config")  # 执行查询SQL
        self.tableSqlQueryModel.setHeaderData(0, Qt.Horizontal, '设备终端编号')
        self.tableSqlQueryModel.setHeaderData(1, Qt.Horizontal, '设备终端名称')
        self.tableSqlQueryModel.setHeaderData(2, Qt.Horizontal, '通信传输协议')
        self.tableSqlQueryModel.setHeaderData(3, Qt.Horizontal, '通信IP地址')
        self.tableSqlQueryModel.setHeaderData(4, Qt.Horizontal, '通信端口地址')
        self.tableSqlQueryModel.setHeaderData(5, Qt.Horizontal, '数据采集频率')
        self.tableSqlQueryModel.setHeaderData(6, Qt.Horizontal, '设备是否启用')

        return self.tableSqlQueryModel

    # 查询监测数据（温度、湿度）数据显示到监测页面，数据制定条数并按正序输出
    def db_tableSqlQuery_temperature_humidity(self, tableNameStr, dateTime, dataNum):
        dateTime
        sql_str = "select *from(" + "select data_temperature, data_humidity, data_time, data_id from " + tableNameStr \
                + " where data_time <= " + "'" + dateTime + "'"  \
                  + " order by data_id desc limit " + str(dataNum) + ") temp order by data_id"      # 查询制定条数据，并正序输出
        self.dataQuery = QSqlQuery(self.db)
        self.dataQuery.exec_(sql_str)       # 执行查询SQL

        return self.dataQuery

    # 关闭数据库
    def db_close(self):
        self.db.close()
