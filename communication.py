"""
    数据通信界面的独立py功能逻辑类 communicationForm

"""

from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt, QSize, pyqtSlot
from PyQt5.QtWidgets import QPushButton, QWidget, QLabel, QMessageBox
from PyQt5.QtSql import QSqlTableModel
from PyQt5.QtGui import QIcon, QPixmap

from ui_communication import Ui_communicationForm


class QmyCommunicationForm(QWidget):
    def __init__(self, myDbOption, parent=None):
        super().__init__(parent)
        self.ui = Ui_communicationForm()
        self.ui.setupUi(self)

        self.commTerminal_lists = []                 # 加载导航栏中可监测设备的对象字典，包括id/name/protocol/ip/port/frequency/state
        self.commTerminal_lists_valid_id = []        # 存储启动通信的设备id
        self.myCommDbClass = myDbOption              # 数据库对象
        self.__currentTerminal_id = -1               # 当前监测的设备ID
        self.__currentTerminal_index = -1            # 当前监测的设备在commTerminal_lists中的索引值
        self.previousBtn = None                      # 保存上一次按下的按键，用于切换按键显示状态

        self.CommNavigationChanged()            # 创建导航栏

    # --------------------------导航栏部分----------------------------------
    # 创建或更新导航栏
    def CommNavigationChanged(self):
        # 删除导航栏中原来的所有控件
        for i in range(self.ui.commBtnContainerFrame.layout().count()):
            self.ui.commBtnContainerFrame.layout().itemAt(i).widget().deleteLater()
        # 控件状态和参数恢复到初始状态
        self.__recover_ui()
        # 查询并添加需要加入的设备按键
        self.commTerminal_lists = self.__getTerminal_info()
        if self.commTerminal_lists is not None:
            for i in range(len(self.commTerminal_lists)):
                btn_t = QPushButton(self.commTerminal_lists[i]['name'], self)
                btn_t.setObjectName(str(self.commTerminal_lists[i]['id']))
                sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
                btn_t.setSizePolicy(sizePolicy)
                btn_t.setMinimumSize(QSize(0, 50))
                btn_t.setMaximumSize(QSize(16777215, 50))
                self.ui.commBtnContainerFrame.layout().addWidget(btn_t, 0, Qt.AlignTop)
                btn_t.clicked.connect(self.do_btn_t_clicked)              # 按键的槽函数，用于通知当前显示检查的设备对象按键执行
        # 增加一个Lab下方空白导航进行填充，是的上面按键可以从上往下排列，并设置相同背景颜色
        commSpacerLab = QLabel(self)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Expanding)
        commSpacerLab.setSizePolicy(sizePolicy)
        commSpacerLab.setStyleSheet("QLabel{background-color: rgb(255, 255, 255);}")
        self.ui.commBtnContainerFrame.layout().addWidget(commSpacerLab)

    # 操作数据库表terminal_config获取已经启用的设备信息
    def __getTerminal_info(self):
        terminal_list = []
        tableModel = QSqlTableModel()    # 数据模型,用于查询数据
        if not self.myCommDbClass.db_table_check("terminal_config"):
            return
        tableModel.setTable("terminal_config")
        tableModel.setFilter("terminal_state=1")
        tableModel.select()
        for i in range(tableModel.rowCount()):
            terminal_dict = {'id': tableModel.record(i).value(0), 'name': tableModel.record(i).value(1), \
                             'protocol': tableModel.record(i).value(2), 'ip': tableModel.record(i).value(3), \
                             'port': tableModel.record(i).value(4), 'frequency': tableModel.record(i).value(5), \
                             'state': tableModel.record(i).value(6)}
            terminal_list.append(terminal_dict)

        return terminal_list

    # 导航部分监测设备按键触发点击事件处理函数
    def do_btn_t_clicked(self):
        if self.previousBtn is not None:
            self.previousBtn.setStyleSheet("color: rgb(0, 0, 0)")       # 恢复上一次按键的状态
            self.previousBtn.setIcon(QIcon())                           # 清空原有按下的图标
        btn_objectName = self.sender().objectName()                     # 获取被按下的按键对象objectName
        self.__currentTerminal_id = int(btn_objectName)
        self.sender().setStyleSheet("color: rgb(255, 0, 0);\n" " font: 10pt Agency FB;")        # 设置当前选定按键的状态
        icon = QIcon()
        icon.addPixmap(QPixmap(":/img/img/directionRight.png"), QIcon.Normal, QIcon.Off)
        self.sender().setIcon(icon)
        self.sender().setIconSize(QSize(12, 12))
        self.previousBtn = self.sender()             # 保存当前选定的按键
        # 切换设备后更新显示信息和通信按键显示状态
        self.__updateTerminal_Info()                 # 更新当前显示的设备信息
        self.__updateTerminal_Btn_show()             # 更新当前设备的通信按键显示效果

    # 自定义槽函数，用于config更改设备状态的信号响应
    def do_signalTerminalStateChanged(self):
        self.CommNavigationChanged()                     # 接收由于config设备状态可能更改发送来的信号，触发该槽函数更新导航栏

    # 更新当前设备信息并显示到前端
    def __updateTerminal_Info(self):
        if self.commTerminal_lists is not None:
            for i in range(len(self.commTerminal_lists)):
                if self.commTerminal_lists[i]['id'] == self.__currentTerminal_id:     # 循环找到当前被选择的设备id
                    self.ui.label_name.setText(self.commTerminal_lists[i]['name'])          # 更新界面
                    self.ui.label_ip.setText(self.commTerminal_lists[i]['ip'])              # 更新界面
                    self.ui.label_port.setText(str(self.commTerminal_lists[i]['port']))     # 更新界面
                    self.__currentTerminal_index = i                                        # 保存当前设备列表中的当前索引

                    return

    # 更新当前设备的通信按键显示效果
    def __updateTerminal_Btn_show(self):
        icon = QIcon()
        if self.__currentTerminal_id in self.commTerminal_lists_valid_id:
            self.ui.openCommBtn.setText("数据采集中...")
            icon.addPixmap(QPixmap(":/img/img/communication_red.png"), QIcon.Normal, QIcon.Off)
            self.ui.openCommBtn.setChecked(True)
        else:
            self.ui.openCommBtn.setText("启动设备采集")
            icon.addPixmap(QPixmap(":/img/img/communication_black.png"), QIcon.Normal, QIcon.Off)
            self.ui.openCommBtn.setChecked(False)
        self.ui.openCommBtn.setIcon(icon)

    # 控件状态和参数恢复到初始状态
    def __recover_ui(self):
        self.previousBtn = None
        self.commTerminal_lists_valid_id.clear()     # 清空所有有效的设备信息
        self.__currentTerminal_id = -1               # 当前监测的设备ID
        self.__currentTerminal_index = -1            # 当前监测的设备在commTerminal_lists中的索引值
        self.__updateTerminal_Btn_show()             # 更新按键状态

    # -------------------------------END----------------------------------

    # --------------------------界面采集启动按键部分-------------------------
    # 通信任务按键启动当前设备采集数据（槽函数）
    @pyqtSlot(bool)
    def on_openCommBtn_clicked(self, checked):
        if self.__currentTerminal_id == -1:
            QMessageBox.information(self, "提示消息", "请选择一个监测设备", QMessageBox.Yes)
            return
        icon = QIcon()
        if checked:
            self.ui.openCommBtn.setText("数据采集中...")
            icon.addPixmap(QPixmap(":/img/img/communication_red.png"), QIcon.Normal, QIcon.Off)
            if self.__currentTerminal_id not in self.commTerminal_lists_valid_id:
                self.commTerminal_lists_valid_id.append(self.__currentTerminal_id)

        else:
            self.ui.openCommBtn.setText("启动设备采集")
            icon.addPixmap(QPixmap(":/img/img/communication_black.png"), QIcon.Normal, QIcon.Off)
            if self.__currentTerminal_id in self.commTerminal_lists_valid_id:
                self.commTerminal_lists_valid_id.remove(self.__currentTerminal_id)
        self.ui.openCommBtn.setIcon(icon)
        self.ui.openCommBtn.setIconSize(QSize(24, 24))

    # -------------------------------END----------------------------------

    # ------------------------采集任务通信的自定义槽函数-----------------------
    def do_signalCommProcess_toUI(self, info):
        self.ui.plainTextEdit.appendPlainText(info)         # 接收数据采集线程将采集过程提升信息发生到界面上

    # -------------------------------END----------------------------------
