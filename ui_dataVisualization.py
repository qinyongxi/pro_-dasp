# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dataVisualization.ui'
#
# Created by: PyQt5 UI code generator 5.15.9
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_dataVisualizationForm(object):
    def setupUi(self, dataVisualizationForm):
        dataVisualizationForm.setObjectName("dataVisualizationForm")
        dataVisualizationForm.resize(798, 393)
        self.verticalLayout = QtWidgets.QVBoxLayout(dataVisualizationForm)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setSpacing(0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.dataVisuBackgroundWidget = QtWidgets.QWidget(dataVisualizationForm)
        self.dataVisuBackgroundWidget.setObjectName("dataVisuBackgroundWidget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.dataVisuBackgroundWidget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setSpacing(0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.dataVisuNavigationWidget = QtWidgets.QWidget(self.dataVisuBackgroundWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.dataVisuNavigationWidget.sizePolicy().hasHeightForWidth())
        self.dataVisuNavigationWidget.setSizePolicy(sizePolicy)
        self.dataVisuNavigationWidget.setMinimumSize(QtCore.QSize(120, 0))
        self.dataVisuNavigationWidget.setMaximumSize(QtCore.QSize(150, 16777215))
        font = QtGui.QFont()
        font.setFamily("Agency FB")
        self.dataVisuNavigationWidget.setFont(font)
        self.dataVisuNavigationWidget.setObjectName("dataVisuNavigationWidget")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.dataVisuNavigationWidget)
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_2.setSpacing(0)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.dataVisuLabTitle = QtWidgets.QLabel(self.dataVisuNavigationWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.dataVisuLabTitle.sizePolicy().hasHeightForWidth())
        self.dataVisuLabTitle.setSizePolicy(sizePolicy)
        self.dataVisuLabTitle.setMinimumSize(QtCore.QSize(0, 50))
        self.dataVisuLabTitle.setMaximumSize(QtCore.QSize(16777215, 50))
        font = QtGui.QFont()
        font.setFamily("方正黑体_GBK")
        font.setPointSize(10)
        self.dataVisuLabTitle.setFont(font)
        self.dataVisuLabTitle.setStyleSheet("color: rgb(255, 255, 255);\n"
"background-color: rgb(60, 60, 60);")
        self.dataVisuLabTitle.setAlignment(QtCore.Qt.AlignCenter)
        self.dataVisuLabTitle.setObjectName("dataVisuLabTitle")
        self.verticalLayout_2.addWidget(self.dataVisuLabTitle)
        self.dataVisuBtnContainerScrollArea = QtWidgets.QScrollArea(self.dataVisuNavigationWidget)
        font = QtGui.QFont()
        font.setFamily("Agency FB")
        self.dataVisuBtnContainerScrollArea.setFont(font)
        self.dataVisuBtnContainerScrollArea.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.dataVisuBtnContainerScrollArea.setFrameShadow(QtWidgets.QFrame.Plain)
        self.dataVisuBtnContainerScrollArea.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.dataVisuBtnContainerScrollArea.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.dataVisuBtnContainerScrollArea.setWidgetResizable(True)
        self.dataVisuBtnContainerScrollArea.setAlignment(QtCore.Qt.AlignCenter)
        self.dataVisuBtnContainerScrollArea.setObjectName("dataVisuBtnContainerScrollArea")
        self.dataVisuBtnScrollAreaWidgetContents = QtWidgets.QWidget()
        self.dataVisuBtnScrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 120, 343))
        font = QtGui.QFont()
        font.setFamily("Agency FB")
        self.dataVisuBtnScrollAreaWidgetContents.setFont(font)
        self.dataVisuBtnScrollAreaWidgetContents.setObjectName("dataVisuBtnScrollAreaWidgetContents")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout(self.dataVisuBtnScrollAreaWidgetContents)
        self.verticalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_3.setSpacing(0)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.dataVisuBtnContainerFrame = QtWidgets.QFrame(self.dataVisuBtnScrollAreaWidgetContents)
        font = QtGui.QFont()
        font.setFamily("Agency FB")
        self.dataVisuBtnContainerFrame.setFont(font)
        self.dataVisuBtnContainerFrame.setFrameShape(QtWidgets.QFrame.Panel)
        self.dataVisuBtnContainerFrame.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.dataVisuBtnContainerFrame.setObjectName("dataVisuBtnContainerFrame")
        self.verticalLayout_4 = QtWidgets.QVBoxLayout(self.dataVisuBtnContainerFrame)
        self.verticalLayout_4.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_4.setSpacing(0)
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.verticalLayout_3.addWidget(self.dataVisuBtnContainerFrame)
        self.dataVisuBtnContainerScrollArea.setWidget(self.dataVisuBtnScrollAreaWidgetContents)
        self.verticalLayout_2.addWidget(self.dataVisuBtnContainerScrollArea)
        self.horizontalLayout.addWidget(self.dataVisuNavigationWidget)
        self.dataVisuContentWidget = QtWidgets.QWidget(self.dataVisuBackgroundWidget)
        self.dataVisuContentWidget.setObjectName("dataVisuContentWidget")
        self.verticalLayout_5 = QtWidgets.QVBoxLayout(self.dataVisuContentWidget)
        self.verticalLayout_5.setContentsMargins(1, 1, 1, 1)
        self.verticalLayout_5.setSpacing(1)
        self.verticalLayout_5.setObjectName("verticalLayout_5")
        self.dataMatplotlibGroupBox = QtWidgets.QGroupBox(self.dataVisuContentWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.dataMatplotlibGroupBox.sizePolicy().hasHeightForWidth())
        self.dataMatplotlibGroupBox.setSizePolicy(sizePolicy)
        self.dataMatplotlibGroupBox.setMinimumSize(QtCore.QSize(0, 80))
        self.dataMatplotlibGroupBox.setMaximumSize(QtCore.QSize(16777215, 100))
        self.dataMatplotlibGroupBox.setTitle("")
        self.dataMatplotlibGroupBox.setObjectName("dataMatplotlibGroupBox")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.dataMatplotlibGroupBox)
        self.horizontalLayout_2.setContentsMargins(1, 0, 1, 0)
        self.horizontalLayout_2.setSpacing(1)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.groupBox = QtWidgets.QGroupBox(self.dataMatplotlibGroupBox)
        self.groupBox.setTitle("")
        self.groupBox.setObjectName("groupBox")
        self.verticalLayout_6 = QtWidgets.QVBoxLayout(self.groupBox)
        self.verticalLayout_6.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_6.setSpacing(0)
        self.verticalLayout_6.setObjectName("verticalLayout_6")
        self.widget = QtWidgets.QWidget(self.groupBox)
        self.widget.setObjectName("widget")
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout(self.widget)
        self.horizontalLayout_3.setContentsMargins(1, 1, 1, 1)
        self.horizontalLayout_3.setSpacing(1)
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.label = QtWidgets.QLabel(self.widget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label.sizePolicy().hasHeightForWidth())
        self.label.setSizePolicy(sizePolicy)
        self.label.setMinimumSize(QtCore.QSize(0, 0))
        self.label.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.horizontalLayout_3.addWidget(self.label)
        self.TerminalName_label = QtWidgets.QLabel(self.widget)
        self.TerminalName_label.setText("")
        self.TerminalName_label.setObjectName("TerminalName_label")
        self.horizontalLayout_3.addWidget(self.TerminalName_label)
        self.verticalLayout_6.addWidget(self.widget)
        self.widget_2 = QtWidgets.QWidget(self.groupBox)
        self.widget_2.setObjectName("widget_2")
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout(self.widget_2)
        self.horizontalLayout_4.setContentsMargins(1, 1, 1, 1)
        self.horizontalLayout_4.setSpacing(1)
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.dataTemperatureShow_btn = QtWidgets.QPushButton(self.widget_2)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.dataTemperatureShow_btn.sizePolicy().hasHeightForWidth())
        self.dataTemperatureShow_btn.setSizePolicy(sizePolicy)
        self.dataTemperatureShow_btn.setObjectName("dataTemperatureShow_btn")
        self.horizontalLayout_4.addWidget(self.dataTemperatureShow_btn)
        self.dataHumidityShow_btn = QtWidgets.QPushButton(self.widget_2)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.dataHumidityShow_btn.sizePolicy().hasHeightForWidth())
        self.dataHumidityShow_btn.setSizePolicy(sizePolicy)
        self.dataHumidityShow_btn.setObjectName("dataHumidityShow_btn")
        self.horizontalLayout_4.addWidget(self.dataHumidityShow_btn)
        self.verticalLayout_6.addWidget(self.widget_2)
        self.horizontalLayout_2.addWidget(self.groupBox)
        self.groupBox_2 = QtWidgets.QGroupBox(self.dataMatplotlibGroupBox)
        self.groupBox_2.setTitle("")
        self.groupBox_2.setObjectName("groupBox_2")
        self.verticalLayout_8 = QtWidgets.QVBoxLayout(self.groupBox_2)
        self.verticalLayout_8.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_8.setSpacing(0)
        self.verticalLayout_8.setObjectName("verticalLayout_8")
        self.widget_4 = QtWidgets.QWidget(self.groupBox_2)
        self.widget_4.setObjectName("widget_4")
        self.horizontalLayout_6 = QtWidgets.QHBoxLayout(self.widget_4)
        self.horizontalLayout_6.setContentsMargins(1, 1, 1, 1)
        self.horizontalLayout_6.setSpacing(1)
        self.horizontalLayout_6.setObjectName("horizontalLayout_6")
        self.label_3 = QtWidgets.QLabel(self.widget_4)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_3.sizePolicy().hasHeightForWidth())
        self.label_3.setSizePolicy(sizePolicy)
        self.label_3.setMinimumSize(QtCore.QSize(0, 0))
        self.label_3.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.label_3.setAlignment(QtCore.Qt.AlignCenter)
        self.label_3.setObjectName("label_3")
        self.horizontalLayout_6.addWidget(self.label_3)
        self.dataCapacitySpinBox = QtWidgets.QSpinBox(self.widget_4)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.dataCapacitySpinBox.sizePolicy().hasHeightForWidth())
        self.dataCapacitySpinBox.setSizePolicy(sizePolicy)
        self.dataCapacitySpinBox.setMinimum(10)
        self.dataCapacitySpinBox.setMaximum(60)
        self.dataCapacitySpinBox.setSingleStep(2)
        self.dataCapacitySpinBox.setObjectName("dataCapacitySpinBox")
        self.horizontalLayout_6.addWidget(self.dataCapacitySpinBox)
        self.realTimeDataShow_btn = QtWidgets.QPushButton(self.widget_4)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.realTimeDataShow_btn.sizePolicy().hasHeightForWidth())
        self.realTimeDataShow_btn.setSizePolicy(sizePolicy)
        self.realTimeDataShow_btn.setObjectName("realTimeDataShow_btn")
        self.horizontalLayout_6.addWidget(self.realTimeDataShow_btn)
        self.verticalLayout_8.addWidget(self.widget_4)
        self.widget_3 = QtWidgets.QWidget(self.groupBox_2)
        self.widget_3.setObjectName("widget_3")
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout(self.widget_3)
        self.horizontalLayout_5.setContentsMargins(1, 1, 1, 1)
        self.horizontalLayout_5.setSpacing(1)
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        self.label_2 = QtWidgets.QLabel(self.widget_3)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_2.sizePolicy().hasHeightForWidth())
        self.label_2.setSizePolicy(sizePolicy)
        self.label_2.setMinimumSize(QtCore.QSize(0, 0))
        self.label_2.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.label_2.setAlignment(QtCore.Qt.AlignCenter)
        self.label_2.setObjectName("label_2")
        self.horizontalLayout_5.addWidget(self.label_2)
        self.dataHistory_dateTimeEdit = QtWidgets.QDateTimeEdit(self.widget_3)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.dataHistory_dateTimeEdit.sizePolicy().hasHeightForWidth())
        self.dataHistory_dateTimeEdit.setSizePolicy(sizePolicy)
        self.dataHistory_dateTimeEdit.setMaximumDateTime(QtCore.QDateTime(QtCore.QDate(2030, 12, 31), QtCore.QTime(23, 59, 59)))
        self.dataHistory_dateTimeEdit.setMinimumDateTime(QtCore.QDateTime(QtCore.QDate(2020, 1, 11), QtCore.QTime(0, 0, 0)))
        self.dataHistory_dateTimeEdit.setObjectName("dataHistory_dateTimeEdit")
        self.horizontalLayout_5.addWidget(self.dataHistory_dateTimeEdit)
        self.historyDataShow_btn = QtWidgets.QPushButton(self.widget_3)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.historyDataShow_btn.sizePolicy().hasHeightForWidth())
        self.historyDataShow_btn.setSizePolicy(sizePolicy)
        self.historyDataShow_btn.setObjectName("historyDataShow_btn")
        self.horizontalLayout_5.addWidget(self.historyDataShow_btn)
        self.verticalLayout_8.addWidget(self.widget_3)
        self.horizontalLayout_2.addWidget(self.groupBox_2)
        self.horizontalLayout_2.setStretch(0, 1)
        self.horizontalLayout_2.setStretch(1, 2)
        self.verticalLayout_5.addWidget(self.dataMatplotlibGroupBox)
        self.dataMatplotlibWidget = QtWidgets.QWidget(self.dataVisuContentWidget)
        self.dataMatplotlibWidget.setObjectName("dataMatplotlibWidget")
        self.verticalLayout_7 = QtWidgets.QVBoxLayout(self.dataMatplotlibWidget)
        self.verticalLayout_7.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_7.setSpacing(0)
        self.verticalLayout_7.setObjectName("verticalLayout_7")
        self.verticalLayout_5.addWidget(self.dataMatplotlibWidget)
        self.horizontalLayout.addWidget(self.dataVisuContentWidget)
        self.verticalLayout.addWidget(self.dataVisuBackgroundWidget)

        self.retranslateUi(dataVisualizationForm)
        QtCore.QMetaObject.connectSlotsByName(dataVisualizationForm)

    def retranslateUi(self, dataVisualizationForm):
        _translate = QtCore.QCoreApplication.translate
        dataVisualizationForm.setWindowTitle(_translate("dataVisualizationForm", "Form"))
        self.dataVisuLabTitle.setText(_translate("dataVisualizationForm", "数据可视化"))
        self.label.setText(_translate("dataVisualizationForm", "监测设备："))
        self.dataTemperatureShow_btn.setText(_translate("dataVisualizationForm", "显示温度数据"))
        self.dataHumidityShow_btn.setText(_translate("dataVisualizationForm", "显示湿度数据"))
        self.label_3.setText(_translate("dataVisualizationForm", "选择显示数据容量大小："))
        self.realTimeDataShow_btn.setText(_translate("dataVisualizationForm", "监测实时数据"))
        self.label_2.setText(_translate("dataVisualizationForm", "选择历史数据截止时间："))
        self.historyDataShow_btn.setText(_translate("dataVisualizationForm", "显示历史数据"))
