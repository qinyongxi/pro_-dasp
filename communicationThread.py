"""
    数据采集任务，单独的线程进行数据采集
"""

from PyQt5.QtCore import QThread, pyqtSignal, QDateTime, pyqtSlot, QMutex
from PyQt5.QtNetwork import QUdpSocket, QHostAddress
from PyQt5.QtSql import QSqlTableModel
from re import findall
# 导入全局变量
import globalVar as gl
gl._init()
# 全局互斥变量
myQmutex = QMutex()

class QmyCommThread(QThread):
    signalThread_crashed = pyqtSignal(Exception)      # 定义一个线程崩溃信号
    signalCommProcess_toUI = pyqtSignal(str)          # 定义一个通信信号，将数据采集过程中需要再界面上显示的信息传递给界面线前段类槽
    signalCommDataReceive_in_storge = pyqtSignal(str) # 定义一个通信信号，当设备采集新数据成功，通知监测界面，实时更新实时监测数据
    def __init__(self, commTerminal_lists, commTerminal_lists_valid_id, myDbOption, parent=None):
        super().__init__(parent)

        self.commTerminal_lists = commTerminal_lists                      # 当前有效监测设备的信息
        self.commTerminal_lists_valid_id = commTerminal_lists_valid_id    # 当前启动的监测设备的id信息
        self.__currentCommTerminal_id = 0                                 # 存储当前正在采集的设备id信息
        self.__currentCommTerminal_name = ''                              # 存储当前正在采集的设备名称信息

        self.myDbClass = myDbOption
        self.updSocket = QUdpSocket(self)

    def run(self):
        try:
            while True:
                self.do_dataCommProcess()
                QThread.sleep(gl.get_value("dataCollectTaskLoop_time"))
        except Exception as e:
            # 发生线程崩溃的信号
            self.signalCommProcess_toUI.emit(e)
            self.signalThread_crashed.emit(e)

    # 数据采集任务
    def do_dataCommProcess(self):
        nowTime = QDateTime.currentDateTime()
        self.signalCommProcess_toUI.emit("启动采集数据任务，采集时间：{0}\n".format(nowTime.toString("yyyy-MM-dd hh:mm:ss")))
        if self.commTerminal_lists_valid_id is not None:
            if len(self.commTerminal_lists_valid_id) == 0:
                self.signalCommProcess_toUI.emit("当前无设备数据采集需求，请配置采集！\n")
                return
            for i in range(len(self.commTerminal_lists_valid_id)):                     # 循环实现对多个设备数据的采集
                self.__currentCommTerminal_id = self.commTerminal_lists_valid_id[i]
                self.__currentCommTerminal_name, comm_ip, comm_port = self.__find_name_ip_port(self.__currentCommTerminal_id)
                self.__dataCollect_Process(comm_ip, comm_port)
                QThread.msleep(gl.get_value("terminalCollectInterval_time"))  # 每一个设备采集完成后，等待间隔时间采集下一个设备

            self.signalCommProcess_toUI.emit("----------------任务处理完毕--------------------\n")

    # 通过设备id查询采集设备的名称、ip和port值
    def __find_name_ip_port(self, valid_id):
        if self.commTerminal_lists is not None:
            for i in range(len(self.commTerminal_lists)):
                if valid_id == self.commTerminal_lists[i]['id']:     # 根据id找到当前采集设备的信息
                    return self.commTerminal_lists[i]['name'], self.commTerminal_lists[i]['ip'], self.commTerminal_lists[i]['port']

    # 单个设备数据采集处理
    def __dataCollect_Process(self, remote_ip, remote_port):
            # 注意要先配置接收，再发送数据，如果反过来则无法接收到数据
            self.updSocket = QUdpSocket(self)
            self.updSocket.bind(QHostAddress(remote_ip), remote_port)
            self.updSocket.readyRead.connect(self.on_readRead)

            dataRequest = "Request_data".encode()
            self.updSocket.writeDatagram(dataRequest, QHostAddress(remote_ip), remote_port)
            self.signalCommProcess_toUI.emit("发送采集请求，请求设备：{0}".format(self.__currentCommTerminal_name))
            self.updSocket.waitForReadyRead(gl.get_value("waitForReadyRead_time"))        # 阻塞等待接收数据,ms
            self.updSocket.close()

    # 网络UPD接收数据
    @pyqtSlot()
    def on_readRead(self):
        while self.updSocket.hasPendingDatagrams():       # 判断是否接收到数据
            data, host, port = self.updSocket.readDatagram(self.updSocket.pendingDatagramSize())
            self.signalCommProcess_toUI.emit("接收到IP：{0}, 端口：{1}，发送的数据：{2}".format(host.toString(), str(port), data.decode()))
            dataReceive = self.__dataReceiveParse(data.decode(), host.toString(), str(port))
            self.__dataReceive_in_storge(dataReceive)

    # 接收数据解析
    def __dataReceiveParse(self, dataStr, ipStr, portStr):
        # datalist格式：设备id, 设备名称、设备ip、设备端口、数据1、数据2、数据3.....
        datalist = []
        terminal_id, terminal_name = self.__find_id_name_form_ip_port(ipStr, portStr)
        datalist.append(terminal_id)
        datalist.append(terminal_name)
        datalist.append(ipStr)
        datalist.append(portStr)
        dataStr = str(dataStr.strip())
        if not dataStr.startswith('###'):     # 数据包未以###开头
            datalist.append('-1')             # 数据错误，赋-1反回
            datalist.append('-1')
            return
        datasParse = findall("\d+\.\d+", dataStr)
        if datasParse is None or len(datasParse) != 2:
            datalist.append('-1')  # 数据错误，赋-1反回
            datalist.append('-1')
            return
        else:
            datalist.append(datasParse[0])
            datalist.append(datasParse[1])
        return datalist

    # 通过设备ip/port查询采集设备id,设备名称
    def __find_id_name_form_ip_port(self, ipStr, portStr):
        if self.commTerminal_lists is not None:
            for i in range(len(self.commTerminal_lists)):
                if self.commTerminal_lists[i]["ip"] == ipStr and self.commTerminal_lists[i]['port'] == int(portStr):
                    return self.commTerminal_lists[i]['id'], self.commTerminal_lists[i]['name']


    # 接收数据入库
    def __dataReceive_in_storge(self, datalist):      # datalist格式：设备id, 设备名称、设备ip、设备端口、数据1、数据2、数据3.....
        # 数据并入库
        terminal_id = int(datalist[0])
        terminal_name = str(datalist[1])
        terminal_ip = str(datalist[2])
        terminal_port = int(datalist[3])
        temperature = float(datalist[4])
        humidity = float(datalist[5])
        nowDateTime = QDateTime.currentDateTime().toString("yyyy-MM-dd hh:mm:ss")
        # 判断接收到的设备ID是否有效
        if terminal_id not in self.commTerminal_lists_valid_id:
            self.signalCommProcess_toUI.emit("错误：接收数据包的设备ID不在有效采集设备序列中")
            return
        # 判断当月的数据表是否存在，若不存在则新建当月数据表，实现数据分月存储，一个月一张数据表
        year = nowDateTime[:4]
        month = nowDateTime[5:7]
        data_Table_name = "data_{0}_{1}".format(year, month)               # 当月数据表名称，例如：data_2023_05
        if not self.myDbClass.db_table_check(data_Table_name):
            self.__creator_data_table(data_Table_name)
        # 数据入库
        myQmutex.lock()      # 操作数据库加锁
        data_tableModel = QSqlTableModel(self, self.myDbClass.db)          # 局部变量用于插入新的数据
        data_tableModel.setTable(data_Table_name)
        data_tableModel.setEditStrategy(QSqlTableModel.OnManualSubmit)
        data_tableModel.select()
        data_rowCount = data_tableModel.rowCount()
        data_tableModel.insertRow(data_rowCount)
        data_tableModel.setData(data_tableModel.index(data_rowCount, 1), int(terminal_id))
        data_tableModel.setData(data_tableModel.index(data_rowCount, 2), float(temperature))
        data_tableModel.setData(data_tableModel.index(data_rowCount, 3), float(humidity))
        data_tableModel.setData(data_tableModel.index(data_rowCount, 4), nowDateTime)
        if data_tableModel.submitAll():
            data_tableModel.database().commit()
            self.signalCommProcess_toUI.emit("数据入库成功---设备：{0}，ip:{1}, port: {2}, 温度：{3}， 湿度：{4}，时间：{5}".format(\
                terminal_name, terminal_ip, terminal_port, temperature, humidity, nowDateTime))
            self.signalCommDataReceive_in_storge.emit("数据采集成功，采集设备为：{0}".format(terminal_id))
        else:
            self.signalCommProcess_toUI.emit("数据入库失败---设备：{0}".format(self.__currentCommTerminal_name))
        myQmutex.unlock()     # 操作数据库完成解锁

    # 新建当月数据表格
    def __creator_data_table(self, data_Table_name):
        try:
            sql_str = "CREATE TABLE [{0}]( [data_id] INTEGER PRIMARY KEY ASC AUTOINCREMENT NOT NULL, \
                                                  [terminal_id] INTEGER, \
                                                  [data_temperature] FLOAT, \
                                                  [data_humidity] FLOAT, \
                                                  [data_time] TEXT);".format(data_Table_name)
            self.myDbClass.db_QSqlQuery(sql_str)
        except Exception as e:
            self.signalCommProcess_toUI.emit(e)


